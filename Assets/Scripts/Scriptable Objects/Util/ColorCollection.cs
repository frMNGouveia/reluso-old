﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * A scriptable object to store color collections to be used by the game. This allows colors to be changed in editor without needing to change code.
*/
using UnityEngine;

[CreateAssetMenu(fileName = "New Color Collection", menuName = "Colors/Color Collection")]
public class ColorCollection : ScriptableObject
{

    #region Variable Fields
    public Color[] colorCollection;
    #endregion
	
}

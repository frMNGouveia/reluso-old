using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableObjectWithID : ScriptableObject
{
    
    #region Variable Fields

    public int internalID;

    #endregion

}

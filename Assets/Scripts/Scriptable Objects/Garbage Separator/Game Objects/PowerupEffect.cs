using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PowerupEffect : ScriptableObject, IPowerupEffect
{

    public abstract void ActivatePowerup(IGarbageSeparator separator);

}

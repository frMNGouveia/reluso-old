using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TempPowerupEffect : PowerupEffect, ITempPowerupEffect
{

    public float durationInSeconds;

    public abstract void DeactivatePowerup(IGarbageSeparator separator);

}

using UnityEngine;
using System;
using static ScriptablesUtil;

public class Item : ScriptableObjectWithID
{

    #region Variable Fields

    public Sprite sprite;

    #endregion

    #region Public Fields

    public void OnValidate()
    {
        if (ScriptableObjectsWithID(internalID, ALL_ITEMS) > 1)
            Debug.LogError(String.Format(ERROR_REPEATED_ID_MESSAGE, this.name, this.GetType(), this.internalID));
    }

    #endregion

}

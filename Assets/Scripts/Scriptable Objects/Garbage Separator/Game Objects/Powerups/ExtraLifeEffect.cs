using UnityEngine;

[CreateAssetMenu(fileName = "New Extra Life Effect", menuName = "Objects/Effects/New Extra Life Effect")]
public class ExtraLifeEffect : PowerupEffect
{

    public int extraLives;

    public override void ActivatePowerup(IGarbageSeparator separator)
    {
        separator.SetLives(separator.GetLivesLeft() + extraLives);
    }

}

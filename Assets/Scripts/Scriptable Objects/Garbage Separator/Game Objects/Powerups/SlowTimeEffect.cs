using UnityEngine;

[CreateAssetMenu(fileName = "New Slow Time Effect", menuName = "Objects/Effects/New Slow Time Effect")]
public class SlowTimeEffect : TempPowerupEffect
{

    public float timeDivisor;

    public override void ActivatePowerup(IGarbageSeparator separator)
    {
        Time.timeScale /= timeDivisor;
    }

    public override void DeactivatePowerup(IGarbageSeparator separator)
    {
        Time.timeScale *= timeDivisor;
    }

}

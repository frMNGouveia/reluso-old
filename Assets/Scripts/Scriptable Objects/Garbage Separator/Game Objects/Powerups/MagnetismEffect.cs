using UnityEngine;

[CreateAssetMenu(fileName = "New Magnetism Effect", menuName = "Objects/Effects/New Magnetism Effect")]
public class MagnetismEffect : TempPowerupEffect
{

    public float rangeInUnits;

    public override void ActivatePowerup(IGarbageSeparator separator)
    {

    }

    public override void DeactivatePowerup(IGarbageSeparator separator)
    {

    }

}

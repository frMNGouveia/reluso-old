﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * This represents a TrashItem that can be used in game, including its classification (TrashType) and Sprite.
*/
using UnityEngine;
using static ScriptablesUtil;

/// <summary>
/// The <c>TrashItem</c> class represents a TrashItem that can be used in game, including its classification (TrashType) and Sprite.
/// </summary>
[CreateAssetMenu(fileName = "New Trash Item", menuName = "Objects/Items/New Trash Item")]
public class TrashItem : Item
{

    #region Variable Fields
    
    public TrashType trashType;

    #endregion

}

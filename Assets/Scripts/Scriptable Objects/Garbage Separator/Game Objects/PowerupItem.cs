/**
 * Created By: Francisco Miguel Nunes Gouveia
 * This represents a ScriptableObject Powerup that can be used in game, and contains its id, sprite and behavior.
*/
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The <c>Powerup</c> class represents a ScriptableObject Powerup that can be used in game, and contains its id, sprite and behavior.
/// </summary>
[CreateAssetMenu(fileName = "New Powerup Item", menuName = "Objects/Items/New Powerup Item")]
public class PowerupItem : Item
{

    #region Variable Fields

    public PowerupEffect effect;

    //TODO: Validate if Behaviour is TempPowerupBehavior
    #endregion

}

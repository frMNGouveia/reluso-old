﻿/**
* Created By: Francisco Miguel Nunes Gouveia
* A scriptable object that stores information that determines the game's difficulty.
* This class stores a starting speed and a speed change.
* The speed change determines the number by which the speed is multiplied as well as the interval of time at which it is changed.
*/
using System;
using System.Collections.Generic;
using UnityEngine;
using static ScriptablesUtil;

/// <summary>
/// The <c>StaticDifficulty</c> class represents a scriptable object that stores information that determines the game's difficulty.
/// This class stores a starting speed and a speed change.
/// The speed change determines the number by which the speed is multiplied as well as the interval of time at which it is changed.
/// </summary>
[CreateAssetMenu(fileName = "New Regular Difficulty", menuName = "Configurations/Separator/Difficulty/Regular Difficulty")]
public class Difficulty : ScriptableObjectWithID
{

    #region Constant Fields

    public const int DEFAULT_NUMBER_OF_LIVES = 3;
    public const float DEFAULT_STARTING_SPEED = 1.0f;

    #endregion

    #region Variable Fields

    //Starting number of lives
    public int numberOfLives;
    //The starting speed of the game
    public float startingSpeedMultiplier;

    public bool repeatChanges;

    public SpeedChange[] speedChanges;

    #endregion


    #region Public Methods

    public List<TimedAction> GenerateSpeedChanges(IGarbageSeparator separator)
    {
        List<TimedAction> actions = new List<TimedAction>();
        foreach(SpeedChange change in speedChanges)
            actions.Add(TimedAction.CreateAction(() => separator.MultiplySpeed(change.speedMult), change.delay));
        return actions;
    }

    public void OnValidate()
    {
        if (ScriptableObjectsWithID(internalID, ALL_DIFICULTIES) > 1)
            Debug.LogError(String.Format(ERROR_REPEATED_ID_MESSAGE, this.name, this.GetType(), this.internalID));
        CheckDefaultValues();
    }

    #endregion

    #region Private Methods

    private void CheckDefaultValues()
    {
        if (numberOfLives == default || numberOfLives <= 0)
            numberOfLives = DEFAULT_NUMBER_OF_LIVES;
        if (startingSpeedMultiplier == default || startingSpeedMultiplier <= 0)
            startingSpeedMultiplier = DEFAULT_STARTING_SPEED;
    }

    #endregion

}

/**
 * Created By: Francisco Miguel Nunes Gouveia
 * This represents a group/set of containers, used to determine the layout of containers on a level.
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ScriptablesUtil;

/// <summary>
/// The <c>ContainerConfig</c> class represents a group/set of containers, used to determine the layout of containers on a level.
/// </summary>
[CreateAssetMenu(fileName = "New Container Configuration", menuName = "Configurations/Separator/Container/Container Configuration")]
public class ContainerConfig : ScriptableObjectWithID, IComparable
{

    #region Variable Fields
    
    public Container[] containers;

    #endregion

    #region Public Methods

    public int CompareTo(object obj)
    {
        if (obj == null)
            return 1;
        if (obj.GetType() != typeof(Difficulty))
            throw new NotImplementedException();
        Difficulty d = (Difficulty)obj;
        return internalID - d.internalID;
    }

    public void OnValidate()
    {
        if (ScriptableObjectsWithID(internalID, ALL_CONTAINER_CONFIGS) > 1)
            Debug.LogError(String.Format(ERROR_REPEATED_ID_MESSAGE, this.name, this.GetType(), this.internalID));
    }

    #endregion

}

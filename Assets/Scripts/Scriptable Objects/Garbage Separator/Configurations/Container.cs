/**
 * Created By: Francisco Miguel Nunes Gouveia
 * This represents a type of Recycling Container, its data includes which types of TrashItem can be stored and its color.
*/
using System;
using System.Collections.Generic;
using UnityEngine;
using static ScriptablesUtil;

/// <summary>
/// The <c>Container</c> class represents a type of Recycling Container, its data includes which types of TrashItem can be stored and its color.
/// </summary>
[CreateAssetMenu(fileName = "New Container", menuName = "Objects/Trash/Container")]
public class Container : ScriptableObjectWithID
{

    #region Variable Fields
    
    public TrashType[] containerTypes;
    public Color color;

    #endregion

    #region Public Methods

    private HashSet<TrashType> cache = new HashSet<TrashType>();

    public bool ContainsType(TrashType type)
    {
        if (type == null)
            return false;
        if (cache.Contains(type))
            return true;
        foreach (TrashType r in containerTypes) {
            if (r.Equals(type))
            {
                cache.Add(type);
                return true;
            }
        }
        return false;
    }

    public void OnValidate()
    {
        if (ScriptableObjectsWithID(internalID, ALL_CONTAINERS) > 1)
            Debug.LogError(String.Format(ERROR_REPEATED_ID_MESSAGE, this.name, this.GetType(), this.internalID));
    }

    #endregion

}

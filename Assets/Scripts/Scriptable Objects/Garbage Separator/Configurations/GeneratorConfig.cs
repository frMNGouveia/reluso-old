using System;
using UnityEngine;
using static ScriptablesUtil;

[CreateAssetMenu(fileName = "New Generator Configuration", menuName = "Configurations/Separator/Generator/Generator Configuration")]
public class GeneratorConfig : ScriptableObjectWithID
{

    #region Variable Fields

    public int powerupWeight, trashItemWeight;
    public Item[] passList, blockList;

    #endregion

    #region Public Methods

    public void OnValidate()
    {
        if (ScriptableObjectsWithID(internalID, ALL_SEPARATOR_CONFIGS) > 1)
            Debug.LogError(String.Format(ERROR_REPEATED_ID_MESSAGE, this.name, this.GetType(), this.internalID));
    }

    #endregion

}

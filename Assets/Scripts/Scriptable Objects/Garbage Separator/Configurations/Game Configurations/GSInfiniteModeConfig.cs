using UnityEngine;

[CreateAssetMenu(fileName = "New GSInfiniConfig", menuName = "Configurations/Separator/Garbage Separator/Infinite Mode Config")]
public class GSInfiniteModeConfig : SeparatorConfig
{

    #region Public Methods

    public override void CheckDefaultValues()
    {

    }

    #endregion

}

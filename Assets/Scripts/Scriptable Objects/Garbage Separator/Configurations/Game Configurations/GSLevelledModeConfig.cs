using UnityEngine;

[CreateAssetMenu(fileName = "New GSLevelConfig", menuName = "Configurations/Separator/Garbage Separator/Leveled Mode Config")]
public class GSLevelledModeConfig : SeparatorConfig
{
    
    #region Constant Fields

    public const int DEFAULT_TIMEOUT = 60;

    #endregion

    #region Variable Fields

    public float timeOut;

    #endregion

    #region Public Methods
    
    public override void CheckDefaultValues()
    {
        if (timeOut == default || timeOut == 0f)
            timeOut = DEFAULT_TIMEOUT;
    }

    #endregion

}

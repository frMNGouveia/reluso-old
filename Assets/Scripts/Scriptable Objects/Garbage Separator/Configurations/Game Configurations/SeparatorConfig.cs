/**
 * Created By: Francisco Miguel Nunes Gouveia
 * This is a scriptable object that defines a game's configurations, such as the values of the variables on the initial state.
*/
using UnityEngine;
using System;
using static ScriptablesUtil;

public abstract class SeparatorConfig : ScriptableObjectWithID
{

    #region Variable Fields

    public Difficulty difficulty;
    public ContainerConfig containerConfig;
    public GeneratorConfig generatorConfig;

    #endregion

    #region Public Methods

    public void OnValidate()
    {
        if (ScriptableObjectsWithID(internalID, ALL_SEPARATOR_CONFIGS) > 1)
            Debug.LogError(String.Format(ERROR_REPEATED_ID_MESSAGE, this.name, this.GetType(), this.internalID));
        CheckDefaultValues();
    }

    public abstract void CheckDefaultValues();

    #endregion

}

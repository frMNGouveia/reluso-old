﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * A class that represents a type of trash. This is a classification which can be given to a trash object.
*/
using System;
using UnityEngine;
using static ScriptablesUtil;

/// <summary>
/// The <c>TrashType</c> class represents a type of trash. This is a classification which can be given to a trash object.
/// </summary>
[CreateAssetMenu(fileName = "New Trash Type", menuName = "Objects/Trash Types/New Trash Type")]
public class TrashType : ScriptableObjectWithID, IComparable
{

    #region Variable Fields

    public string type;
    public Sprite sprite;

    #endregion

    #region Public Methods

    public int CompareTo(object obj)
    {
        if (obj == null)
            return 1;
        if (obj.GetType() != typeof(TrashType))
            throw new NotImplementedException();
        TrashType r = (TrashType)obj;
        return internalID - r.internalID;
    }

    public override bool Equals(object other)
    {
        if (other == null)
            return false;
        if (other.GetType() != typeof(TrashType))
            return false;
        TrashType r = (TrashType)other;
        if (r == this)
            return true;
        return r.internalID == internalID;
    }

    public override int GetHashCode()
    {
        return internalID;
    }

    public void OnValidate()
    {
        if(ScriptableObjectsWithID(internalID, ALL_TRASH_TYPES) > 1)
            Debug.LogError(String.Format(ERROR_REPEATED_ID_MESSAGE, this.name, this.GetType(), this.internalID));
    }

    #endregion

}

﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * A utility static class that stores  asset information in constants, such as Paths and filenames.
*/

public static class AssetUtil
{

    #region Texture Paths

    public const string OBJECTS_PATH = "Objects/";
    public const string PREFABS_PATH = "Prefabs/";
    public const string MATERIALS_PATH = "Materials/";
    public const string TEXTURES_PATH = "Textures/";
    public const string COLORS_PATH = TEXTURES_PATH + "Colors/";
    public const string SPRITES_PATH = TEXTURES_PATH + "Sprites/";

    #endregion

    #region Scriptable Objects Paths

    public const string OBJ_CONFIGS = OBJECTS_PATH + "Configurations/";
    public const string OBJ_ITEMS = OBJECTS_PATH + "Items/";
    public const string OBJ_SEPARATOR_CONFIGS = OBJ_CONFIGS + "Garbage Separator/";

    public const string OBJ_CONTAINER_CONFIG_PATH = OBJ_SEPARATOR_CONFIGS + "Container Configs/";
    public const string OBJ_SEPARATOR_CONFIG_PATH = OBJ_SEPARATOR_CONFIGS + "Game Configs/";
    public const string OBJ_GENERATOR_CONFIG_PATH = OBJ_SEPARATOR_CONFIGS + "Generator Configs/";
    public const string OBJ_DIFFICULTY_PATH = OBJ_SEPARATOR_CONFIGS + "Difficulties/";

    public const string OBJ_CONTAINERS_PATH = OBJECTS_PATH + "Containers/";
    public const string OBJ_TRASH_TYPES_PATH = OBJECTS_PATH + "Trash Types/";

    public const string OBJ_TRASH_ITEMS_PATH = OBJ_ITEMS + "Trash Items/";
    public const string OBJ_POWERUP_ITEMS_PATH = OBJ_ITEMS + "Powerup Items/";

    #endregion

    #region Trash Types

    public const string RESOURCE_ICONS_PATH = SPRITES_PATH + "Resource Icons/";
    public const string TRASH_ICON_FILENAME = RESOURCE_ICONS_PATH + "indiff_icon_1";
    public const string GLASS_ICON_FILENAME = RESOURCE_ICONS_PATH + "glass_icon";
    public const string PACKAGE_ICON_FILENAME = RESOURCE_ICONS_PATH + "package_icon";
    public const string CARDBOARD_ICON_FILENAME = RESOURCE_ICONS_PATH + "cardboard_icon";


    #endregion

    #region Trash Items

    public const string TRASH_ITEM_SPRITES_PATH = SPRITES_PATH + "Trash/";

    public const string TRASH_ITEMS_GARBAGE_FILENAME_START = "garbage_";
    public const string TRASH_ITEMS_GLASS_FILENAME_START = "glass_";
    public const string TRASH_ITEMS_PACKAGE_FILENAME_START = "plastic_";
    public const string TRASH_ITEMS_CARDBOARD_FILENAME_START = "paper_";
    public const string TRASH_ITEMS_ORGANIC_FILENAME_START = "organic_";


    #endregion

    #region Prefabs

    public const string UI_DIALOG_PREFAB = PREFABS_PATH + "UI/UIDialogBox";
    public const string FADE_OUT_PANEL_PREFAB = PREFABS_PATH + "Scene Fades/Fade Out Scene";

    #endregion

    #region Materials

    public const string MAT_CONTAINER_NAME = "ContainerMaterial";
    public const string MAT_CONTAINER_PATH = MATERIALS_PATH + MAT_CONTAINER_NAME;

    public const string MAT_LIFE_NAME = "LifeHeartMaterial";
    public const string MAT_LIFE_PATH = MATERIALS_PATH + MAT_LIFE_NAME;

    #endregion

    #region Colors

    public const string COLORS_TRASH_TYPES_NAME = "ContainerColors";
    public const string COLORS_TRASH_TYPES_PATH = COLORS_PATH + "Containers/" + COLORS_TRASH_TYPES_NAME;

    public const string COLORS_LIFE_HEART_NAME = "LifeHeartColors";
    public const string COLORS_LIFE_HEART_PATH = COLORS_PATH + "UI/" + COLORS_LIFE_HEART_NAME;


    #endregion
}

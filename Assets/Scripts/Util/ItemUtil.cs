﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Util class for static methods and asset management for Trash Items
*/
using System.Collections.Generic;
using UnityEngine;
using static AssetUtil;
using static TrashTypeUtil;

public static class ItemUtil
{

    #region Variable Fields

    //Collection of all Resources of type TrashItems
    public static readonly TrashItem[] ALL_TRASH_ITEMS = Resources.LoadAll<TrashItem>(OBJ_TRASH_ITEMS_PATH);
    public static readonly PowerupItem[] ALL_POWERUP_ITEMS = Resources.LoadAll<PowerupItem>(OBJ_POWERUP_ITEMS_PATH);
    public static readonly Item[] ALL_ITEMS = Resources.LoadAll<Item>(OBJ_ITEMS);

    #endregion

    #region Public Methods

    public static Item[] GetItemsForConfiguration(SeparatorConfig config)
    {
        List<Item> itemsList = new List<Item>();
        HashSet<Item> itemsSet = new HashSet<Item>();

        bool[] blockList = new bool[ALL_ITEMS.Length];
        foreach(Item i in config.generatorConfig.blockList)
            blockList[i.internalID] = true;

        bool[] trashTypes = new bool[GetHighestTrashTypeID() + 1];
        foreach (Container c in config.containerConfig.containers)
            foreach (TrashType t in c.containerTypes)
                trashTypes[t.internalID] = true;

        foreach (TrashItem t in ALL_TRASH_ITEMS)
            if (trashTypes[t.trashType.internalID] && !blockList[t.internalID])
                AddItem(itemsList, itemsSet, t);

        foreach(Item i in config.generatorConfig.passList)
            AddItem(itemsList, itemsSet, i);

        return itemsList.ToArray();
    }

    private static void AddItem(List<Item> itemsList, HashSet<Item> itemsSet, Item i)
    {
        if(!itemsSet.Contains(i))
        {
            itemsList.Add(i);
            itemsSet.Add(i);
        }
    }

    #endregion

    #region Private Methods
    


    #endregion

}

using UnityEngine;
using static AssetUtil;
using static BetaGameUtil;

public static class ScriptablesUtil
{


    #region Constant Fields

    public const string ERROR_REPEATED_ID_MESSAGE =
        "{0} has Duplicate ID: There is more than one instance of {1} with the ID {2}";

    public static readonly TrashType[] ALL_TRASH_TYPES = Resources.LoadAll<TrashType>(OBJ_TRASH_TYPES_PATH);
    public static readonly Container[] ALL_CONTAINERS = Resources.LoadAll<Container>(OBJ_CONTAINERS_PATH);
    public static readonly ContainerConfig[] ALL_CONTAINER_CONFIGS = Resources.LoadAll<ContainerConfig>(OBJ_CONTAINER_CONFIG_PATH);
    public static readonly SeparatorConfig[] ALL_SEPARATOR_CONFIGS = Resources.LoadAll<SeparatorConfig>(OBJ_SEPARATOR_CONFIG_PATH);
    public static readonly GeneratorConfig[] ALL_GENERATOR_CONFIGS = Resources.LoadAll<GeneratorConfig>(OBJ_GENERATOR_CONFIG_PATH);
    public static readonly Difficulty[] ALL_DIFICULTIES = Resources.LoadAll<Difficulty>(OBJ_DIFFICULTY_PATH);
    public static readonly Item[] ALL_ITEMS = Resources.LoadAll<Item>(OBJ_ITEMS);

    public static readonly Item[] ALL_TRASH_ITEMS = Resources.LoadAll<Item>(OBJ_TRASH_ITEMS_PATH);
    public static readonly PowerupItem[] ALL_POWERUP_ITEMS = Resources.LoadAll<PowerupItem>(OBJ_POWERUP_ITEMS_PATH);

    #endregion

    #region Public Methods

    public static int ScriptableObjectsWithID(int id, ScriptableObjectWithID[] scriptableObjects)
    {
        int count = 0;
        if (scriptableObjects != null)
            foreach (ScriptableObjectWithID o in scriptableObjects)
                if (o != null && o.internalID == id)
                    count++;
        return count;
    }

    public static SeparatorConfig GetDefaultSeparatorConfig()
    {
        SeparatorConfig separatorConfig = null;
        if (ALL_SEPARATOR_CONFIGS.Length > 0)
        {
            separatorConfig = ALL_SEPARATOR_CONFIGS[0];
            foreach (SeparatorConfig c in ALL_SEPARATOR_CONFIGS)
                if (c.name.Equals(DEFAULT_SEPARATOR_CONFIG_NAME))
                    separatorConfig = c;
        }
        else
            separatorConfig = CreateNewSeparatorConfig();

        return separatorConfig;
    }

    #endregion

    #region Private Methods

    private static SeparatorConfig CreateNewSeparatorConfig()
    {
        Debug.Log("No Separator Configs found, creating one.");
        SeparatorConfig separatorConfig = ScriptableObject.CreateInstance<SeparatorConfig>();
        separatorConfig.difficulty = GetDefaultDifficulty();
        separatorConfig.containerConfig = GetDefaultContainerConfig();
        separatorConfig.generatorConfig = GetDefaultGeneratorConfig();
        return separatorConfig;
    }

    private static Difficulty GetDefaultDifficulty()
    {
        Difficulty defaultDifficulty = null;
        if (ALL_DIFICULTIES.Length > 0)
            defaultDifficulty = ALL_DIFICULTIES[0];
        foreach (Difficulty d in ALL_DIFICULTIES)
            if (d.name.Equals(DEFAULT_DIFFICULTY_NAME))
                defaultDifficulty = d;
        return defaultDifficulty;
    }

    private static ContainerConfig GetDefaultContainerConfig()
    {
        ContainerConfig defaultConfig = null;
        if (ALL_CONTAINER_CONFIGS.Length > 0)
            defaultConfig = ALL_CONTAINER_CONFIGS[0];
        foreach (ContainerConfig c in ALL_CONTAINER_CONFIGS)
            if (c.name.Equals(DEFAULT_CONTAINER_CONFIG_NAME))
                defaultConfig = c;
        return defaultConfig;
    }

    private static GeneratorConfig GetDefaultGeneratorConfig()
    {
        GeneratorConfig defaultConfig = null;
        if (ALL_GENERATOR_CONFIGS.Length > 0)
            defaultConfig = ALL_GENERATOR_CONFIGS[0];
        foreach (GeneratorConfig c in ALL_GENERATOR_CONFIGS)
            if (c.name.Equals(DEFAULT_CONTAINER_CONFIG_NAME))
                defaultConfig = c;
        return defaultConfig;
    }

    #endregion

}

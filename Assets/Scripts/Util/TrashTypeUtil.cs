﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * A utility static class to assist in access to assets related to Trash Types.
*/
using System.Collections.Generic;
using UnityEngine;
using static ScriptablesUtil;

public static class TrashTypeUtil
{

    #region Variable Fields

    private static Dictionary<int, TrashType> trashtypes = new Dictionary<int, TrashType>();
    private static bool isSorted = false;
    private static int highestID = -1;

    #endregion

    #region Public Methods

    public static TrashType GetTrashTypeByID(int id)
    {
        AddToDictionary();
        return trashtypes[id];
    }

    public static int GetHighestTrashTypeID()
    {
        AddToDictionary();
        return highestID;
    }

    #endregion
 
    #region Private Methods

    private static void AddToDictionary()
    {
        if (isSorted)
            return;
        foreach (TrashType r in ALL_TRASH_TYPES)
        {
            trashtypes.Add(r.internalID, r);
            if (r.internalID > highestID)
                highestID = r.internalID;
        }
        isSorted = true;
    }

    #endregion
	
}

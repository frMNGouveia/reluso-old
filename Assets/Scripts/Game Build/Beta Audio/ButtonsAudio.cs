﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsAudio : MonoBehaviour
{
    public static ButtonsAudio instance;

    public Sound[] sounds;

    private Dictionary<string, Sound[]> soundsOfName;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        soundsOfName = new Dictionary<string, Sound[]>();

        foreach (Sound s in sounds)
            s.src = gameObject.AddComponent<AudioSource>();
    }
    
    public static void PressButton()
    {
        if (instance != null)
            instance.PlayRandomSoundNamed("Button");
    }

    public static void PressNewGame()
    {
        if(instance != null)
            instance.PlaySound("New Game");
    }

    public static void PressFinish()
    {
        if (instance != null)
            instance.PlaySound("Finish Creation");
    }

    public static void OpenMenu()
    {
        if (instance != null)
            instance.PlaySound("Menu Open");
    }

    public static void CloseMenu()
    {
        if (instance != null)
            instance.PlaySound("Menu Close");
    }


    private void PlaySound(string name)
    {
        foreach(Sound s in sounds)
        {
            if(s.name.Equals(name))
            {
                PlaySound(s);
                return;
            }
        }
    }


    private void PlayRandomSoundNamed(string name)
    {
        List<Sound> soundList = new List<Sound>();
        Sound[] sounds;
        if (!soundsOfName.TryGetValue(name, out sounds))
        {
            foreach (Sound s in this.sounds)
                if(s.name.Equals(name))
                    soundList.Add(s);
            sounds = soundList.ToArray();
            soundsOfName.Add(name, sounds);
        }
        PlayRandomSound(sounds);
    }

    private void PlayRandomSound(Sound[] sounds)
    {
        if (sounds.Length <= 0)
            return;
        PlaySound(sounds[UnityEngine.Random.Range(0, sounds.Length)]);
    }

    private void PlaySound(Sound s)
    {
        s.src.volume = s.volume;
        s.src.pitch = s.pitch;
        s.src.loop = s.loop;
        s.src.clip = s.clip;
        s.src.Play();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaChangeTrigger : MonoBehaviour
{

    public string enterAreaName, leaveAreaName;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            AudioManager.TransitionToArea(enterAreaName);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            AudioManager.TransitionToArea(leaveAreaName);
        }
    }
}

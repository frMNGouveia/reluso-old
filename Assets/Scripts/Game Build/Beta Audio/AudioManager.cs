﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    const int A_CODE_DIV = 10;

    //Menu Area Codes
    public const int A_MENUS = 0;
    public const int A_MENU_CODE = A_MENUS * A_CODE_DIV;
    public const int A_M_MENU_CODE = A_MENU_CODE + 1;


    //Inside Area Codes
    public const int A_INSIDE = 1;
    public const int A_INSIDE_CODE = A_INSIDE * A_CODE_DIV;
    public const int A_CHAR_HOUSE_CODE = A_INSIDE_CODE + 1;

    //Outside Area Codes
    public const int A_OUTSIDE = 2;
    public const int A_OUTSIDE_CODE = A_OUTSIDE * A_CODE_DIV;
    public const int A_FOREST_CODE = A_OUTSIDE_CODE + 1;
    public const int A_VILLAGE_OUT_CODE = A_OUTSIDE_CODE + 2;
    public const int A_VILLAGE_CODE = A_OUTSIDE_CODE + 3;

    [Range(0f, 1f)]
    public float ambientVolume;

    [Range(0f, 1f)]
    public float musicVolume;

    private const string TRACK_NAME_FOREST = "Forest";
    private const string TRACK_NAME_VILLAGE = "Village";
    private const string TRACK_NAME_HOME = "Home";
    private const string TRACK_NAME_NIGHT = "Night";

    private const string AMBIENT_NAME_FOREST = "Forest Ambiance";
    private const string AMBIENT_NAME_VILLAGE = "Town Ambiance";
    private const string AMBIENT_NAME_HOME = "Interior Ambiance";
    private const string AMBIENT_NAME_NIGHT = "Night Ambiance";
    private const string AMBIENT_NAME_SECRET = "Secret Sound";

    public static float fadeInSpeed = 0.015f;
    public static float fadeOutSpeed = 0.015f;

    public GameObject ambSrcObj, musSrcObj;
    public GameObject forge;

    public Sound[] smithingSounds;

    public Sound[] ambientSounds;
    public Sound[] musicSounds;

    private Sound activeMusic;

    public static AudioManager instance;

    private AudioSource prevSecretSource;

    private static bool[] keepFadingMusic, keepFadingAmbiance;

    private Dictionary<string, Sound[]> musicsByName, ambientsByName;

    private int currentArea;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        //DontDestroyOnLoad(gameObject);

        prevSecretSource = null;

        keepFadingMusic = new bool[2];
        keepFadingAmbiance = new bool[2];

        musicsByName = new Dictionary<string, Sound[]>();
        ambientsByName = new Dictionary<string, Sound[]>();

        AudioSource forgeSrc = forge.GetComponent<AudioSource>();
        foreach(Sound s in smithingSounds)
            s.src = forgeSrc;
        foreach (Sound s in ambientSounds)
            s.src = ambSrcObj.AddComponent<AudioSource>();
        foreach (Sound s in musicSounds)
            s.src = musSrcObj.AddComponent<AudioSource>();
    }
    
    IEnumerator Start()
    {
        yield return StartCoroutine("PlayForgeSound");
    }
    
    void Update()
    {
        
    }

    private IEnumerator PlayForgeSound()
    {
        if (!IsSunOn())
            yield return new WaitForSeconds(1);
        else {
            float secs = UnityEngine.Random.Range(400, 1200);
            yield return new WaitForSeconds(secs / 1000f);
            Sound s = smithingSounds[UnityEngine.Random.Range(0, smithingSounds.Length)];
            s.src.volume = s.volume * ambientVolume;
            s.src.pitch = s.pitch;
            s.src.PlayOneShot(s.clip);
        }
        yield return StartCoroutine("PlayForgeSound");
    }




    public static bool IsInside(int area)
    {
        return area/A_CODE_DIV == A_INSIDE;
    }

    public static void UpdateAreaStatic(int area)
    {
        if (IsSunOn())
        {
            if(area == A_VILLAGE_OUT_CODE)
            {
                FadeOutCaller(instance.activeMusic, keepFadingMusic);
                instance.UpdateOutSkirtsAmbiance(area);
            }
            else
            {
                Debug.Log("Not in outskirts.");
                switch (area)
                {
                    case A_VILLAGE_CODE:
                        instance.UpdateOutSkirtsAmbiance(area);
                        instance.SwapMusic(TRACK_NAME_VILLAGE);
                        break;
                    case A_CHAR_HOUSE_CODE:
                        instance.PlayAmbiance(AMBIENT_NAME_HOME);
                        instance.SwapMusic(TRACK_NAME_HOME);
                        break;
                    case A_FOREST_CODE:
                        if(instance.currentArea == A_VILLAGE_OUT_CODE)
                            instance.UpdateOutSkirtsAmbiance(area);
                        else
                            instance.PlayAmbiance(AMBIENT_NAME_FOREST);
                        instance.SwapMusic(TRACK_NAME_FOREST);
                        break;
                }
            }
        }
        else
        {
            if(instance.currentArea == A_CHAR_HOUSE_CODE && !IsInside(area))
                instance.PlayAmbiance(AMBIENT_NAME_NIGHT);
            if(IsInside(area))
                instance.ClearAllAmbiance();
        }
        instance.currentArea = area;
    }

    private void UpdateOutSkirtsAmbiance(int area) {
        Sound f = ClipWithName(AMBIENT_NAME_FOREST, ambientSounds);
        Sound t = ClipWithName(AMBIENT_NAME_VILLAGE, ambientSounds);
        if (f == null || t == null)
            return;
        switch (area)
        {
            case A_VILLAGE_CODE:
                FadeOutCaller(f, keepFadingAmbiance);
                UpdateSoundSrc(t, ambientVolume);
                break;
            case A_VILLAGE_OUT_CODE:
                UpdateSoundSrc(f, 0.4f * ambientVolume);
                UpdateSoundSrc(t, 0.6f * ambientVolume);
                if(currentArea == A_VILLAGE_CODE)
                    f.src.Play();
                if (currentArea == A_FOREST_CODE)
                    t.src.Play();
                break;
            case A_FOREST_CODE:
                FadeOutCaller(t, keepFadingAmbiance);
                UpdateSoundSrc(f, ambientVolume);
                break;
        }
    }


    public static void PlaySecretSound(AudioSource src)
    {
        instance.SecretSound(src);
    }

    private void SecretSound(AudioSource src)
    {
        if(!src.Equals(prevSecretSource))
        {
            Sound s = ClipWithName(AMBIENT_NAME_SECRET, ambientSounds);
            src.volume = instance.ambientVolume * s.volume;
            src.PlayOneShot(s.clip);
            prevSecretSource = src;
        }
    }

    private static Sound ClipWithName(string name, Sound[] sounds)
    {
        foreach (Sound s in sounds)
            if (s.name.Equals(name))
                return s;
        return null;
    }

    public static void TransitionToArea(string areaName)
    {
        switch(areaName) {
            case "VillageIn":
            case "VillageOut":
                //PlayerCharacter.UpdateArea(A_VILLAGE_OUT_CODE);
                break;
            case "Village":
                //PlayerCharacter.UpdateArea(A_VILLAGE_CODE);
                break;
            case "Forest":
            default:
                //PlayerCharacter.UpdateArea(A_FOREST_CODE);
                break;
        }
    }

    public static void TimeOfDayTransition()
    {
        //if(IsSunOn())
        //    UpdateAreaStatic(PlayerCharacter.data.area);
        //else
        //{
            instance.SwapMusic(TRACK_NAME_NIGHT);
            instance.ClearAllAmbiance();
            //if (!IsInside(PlayerCharacter.data.area))
            //    instance.PlayAmbiance(AMBIENT_NAME_NIGHT);
        //}

    }

    private void ClearAllAmbiance()
    {
        foreach (Sound s in ambientSounds)
            if (s.src.isPlaying)
                s.src.Stop();
    }

    private void PlayAmbiance(string name)
    {
        instance.ClearAllAmbiance();
        Sound s = ClipWithName(name, ambientSounds);
        if(s != null)
        {
            UpdateSoundSrc(s, ambientVolume);
            FadeInCaller(s, keepFadingAmbiance);
        }
    }

    private void StopAmbiance(string name)
    {
        Sound s = ClipWithName(name, ambientSounds);
        if (s != null)
            s.src.Stop();
    }

    private static bool IsSunOn()
    {
        //return PlayerCharacter.data.isSunOn;
        return false;
    }

    private void SwapMusic(string name)
    {
        Sound s = RandomSoundWithName(name, musicSounds, musicsByName);
        if (s == null)
            return;
        if(activeMusic != null)
        {
            if (s.src.Equals(activeMusic.src))
                return;

            if (activeMusic.src.isPlaying)
                FadeOutCaller(activeMusic, keepFadingMusic);
        }
        instance.StartCoroutine(QueueMusic(s));
    }

    public static void UpdateSoundSrc(Sound s, float volumeMod)
    {
        s.src.playOnAwake = false;
        s.src.pitch = s.pitch;
        s.src.clip = s.clip;
        s.src.loop = s.loop;
        s.src.volume = s.volume * volumeMod;
    }

    public IEnumerator QueueMusic(Sound s)
    {
        if(activeMusic != null)
            while(activeMusic.src.isPlaying)
                yield return new WaitForSeconds(0.1f);
        activeMusic = s;
        UpdateSoundSrc(s, musicVolume);
        FadeInCaller(s, keepFadingMusic);
    }

    public static void FadeInCaller(Sound s, bool[] keepFadingStatus)
    {
        instance.StartCoroutine(FadeIn(s, keepFadingStatus));
    }

    public static void FadeOutCaller(Sound s, bool[] keepFadingStatus)
    {
        instance.StartCoroutine(FadeOut(s, keepFadingStatus));
    }

    private Sound RandomSoundWithName(string name, Sound[] sounds, Dictionary<string, Sound[]> dic)
    {
        Sound[] soundArray;
        if(!dic.TryGetValue(name, out soundArray))
        {
            List<Sound> soundList = new List<Sound>();
            foreach (Sound s in sounds)
                if (s.name.Equals(name))
                    soundList.Add(s);
            soundArray = soundList.ToArray();
            dic.Add(name, soundArray);
        }
        if(soundArray == null || soundArray.Length <= 0)
            return null;
        return soundArray[UnityEngine.Random.Range(0, soundArray.Length)];
    }

    private static IEnumerator FadeIn(Sound s, bool[] keepFadingStatus)
    {
        float maximumVolume = s.src.volume;
        keepFadingStatus[0] = true;
        keepFadingStatus[1] = false;


        float vol = 0;
        s.src.volume = vol;
        s.src.Play();
        while (s.src.volume < maximumVolume && keepFadingStatus[0])
        {
            vol += fadeInSpeed;
            if (vol > maximumVolume)
                vol = maximumVolume;
            s.src.volume = vol;
            
            yield return new WaitForSeconds(0.1f);
        }

    }

    private static IEnumerator FadeOut(Sound s, bool[] keepFadingStatus)
    {
        keepFadingStatus[0] = false;
        keepFadingStatus[1] = true;


        float vol = s.src.volume;
        while (s.src.volume > 0 && keepFadingStatus[1])
        {
            vol -= fadeOutSpeed;
            if (vol < 0)
                vol = 0;
            s.src.volume = vol;
            yield return new WaitForSeconds(0.1f);
        }
        s.src.Stop();
    }
}

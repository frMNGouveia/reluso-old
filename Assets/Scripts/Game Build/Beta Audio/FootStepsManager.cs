﻿using UnityEngine;

public class FootStepsManager : MonoBehaviour
{

    private TerrainDetector td;
    private AudioSource src;

    [SerializeField]
    private AudioClip[] runningWoodClips;
    [SerializeField]
    private AudioClip[] runningGrassClips;
    [SerializeField]
    private AudioClip[] runningDirtClips;

    [SerializeField]
    private AudioClip[] sprintingWoodClips;
    [SerializeField]
    private AudioClip[] sprintingGrassClips;
    [SerializeField]
    private AudioClip[] sprintingDirtClips;

    void Awake()
    {
        src = GetComponent<AudioSource>();
        td = new TerrainDetector();
    }

    void RunStep()
    {
        Vector3 pos = new Vector3(transform.position.x, 0, transform.position.z);
        int terrain = td.GetActiveTerrainTextureIdx(pos);
        src.PlayOneShot(GetRandomRunningClip(terrain));
    }

    void SprintStep()
    {
        Vector3 pos = new Vector3(transform.position.x, 0, transform.position.z);
        int terrain = td.GetActiveTerrainTextureIdx(pos);
        src.PlayOneShot(GetRandomSprintingClip(terrain));
    }

    private AudioClip GetRandomRunningClip(int terrain)
    {
        if (IsInside())
            return GetRandomClip(runningWoodClips);
        else if (terrain == 10)
            return GetRandomClip(runningDirtClips);

        return GetRandomClip(runningGrassClips);
    }

    private AudioClip GetRandomSprintingClip(int terrain)
    {
        if (IsInside())
            return GetRandomClip(sprintingWoodClips);
        else if(terrain == 10)
            return GetRandomClip(sprintingDirtClips);

        return GetRandomClip(sprintingGrassClips);
    }

    private AudioClip GetRandomClip(AudioClip[] selection)
    {
        return selection[UnityEngine.Random.Range(0, selection.Length)];
    }

    private bool IsInside()
    {
        return true;
    }

}

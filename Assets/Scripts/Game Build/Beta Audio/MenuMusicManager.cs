﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMusicManager : MonoBehaviour
{
    public static MenuMusicManager instance;

    public Sound menuMusic;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        StartMusic();
    }

    private static void StartMusic()
    {
        AudioSource src = instance.gameObject.AddComponent<AudioSource>();
        instance.menuMusic.src = src;
        src.volume = instance.menuMusic.volume;
        src.pitch = instance.menuMusic.pitch;
        src.clip = instance.menuMusic.clip;
        src.loop = instance.menuMusic.loop;
        src.Play();
    }

    public static void EndMusic()
    {
        if(instance != null)
        {
            AudioSource src = instance.menuMusic.src;
            if(src != null)
            {
                src.Stop();
                Destroy(src);
            }
            Destroy(instance);
            instance = null;
        }
    }
}

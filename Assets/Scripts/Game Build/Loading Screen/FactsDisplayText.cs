using System.Collections;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class FactsDisplayText : MonoBehaviour
{

    #region Constant Fields

    private const float FADE_TIME = 3f;
    private const float WAIT_TIME = FADE_TIME + 0.1f;

    private const float TRANSPARENT_ALPHA = 0.0f;
    private const float SOLID_ALPHA = 1.0f;

    #endregion

    #region Variable Fields

    private TextMeshProUGUI textDisplay;

    private bool isFadedOut;

    private string fact;

    #endregion

    #region Unity Methods

    void Start()
    {
        textDisplay = GetComponent<TextMeshProUGUI>();
        textDisplay.text = "";
        isFadedOut = true;
        textDisplay.CrossFadeAlpha(TRANSPARENT_ALPHA, 0f, true);
    }

    #endregion

    #region Public Methods

    public void DisplayFact(string fact)
    {
        this.fact = fact;
        if (!isFadedOut)
        {
            StartCoroutine(FadeOut());
        }
        else
        {
            textDisplay.text = fact;
            FadeIn();
        }
    }

    #endregion

    #region Private Methods

    private IEnumerator FadeOut()
    {
        textDisplay.CrossFadeAlpha(TRANSPARENT_ALPHA, FADE_TIME, true);
        yield return new WaitForSeconds(WAIT_TIME);
        isFadedOut = true;
        DisplayFact(fact);
    }

    private void FadeIn()
    {
        textDisplay.CrossFadeAlpha(SOLID_ALPHA, FADE_TIME, true);
        isFadedOut = false;
    }

    #endregion

}

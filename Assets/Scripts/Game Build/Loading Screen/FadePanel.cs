using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class FadePanel : MonoBehaviour
{

    #region Constant Fields
    
    private const float FADE_TIME = 1.2f;
    public const float FADE_WAIT = FADE_TIME + 0.05f;

    #endregion

    #region Variable Fields

    public float waitBeforeFade;
    public float startingAlpha;
    public float endingAlpha;

    public bool destroy;

    public bool Done { get; private set; }

    private Image panelImage;

    #endregion

    #region Unity Methods

    void Start()
    {
        Done = false;
        panelImage = GetComponent<Image>();
        panelImage.CrossFadeAlpha(startingAlpha, 0.0f, true);
        StartCoroutine(FadeInScene());
    }

    #endregion

    #region Private Methods

    private IEnumerator FadeInScene()
    {
        yield return new WaitForSeconds(waitBeforeFade);
        panelImage.CrossFadeAlpha(endingAlpha, FADE_TIME, true);
        yield return new WaitForSeconds(FADE_WAIT);
        Done = true;
        if (destroy)
            Destroy(this.gameObject);
    }

    #endregion

}

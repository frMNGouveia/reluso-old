using System.Collections;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class LoadedTextAnimation : MonoBehaviour
{

    #region Constant Fields

    private const float COLOR_STEP = 0.1f;
    private const float WAIT_PERIOD = 0.1f;

    #endregion

    #region Variable Fields

    public Color mainColor;
    public Color darkerColor;
    public Color lighterColor;

    private TextMeshProUGUI text;
    private bool isGettingLighter;

    private float colorPercentage;

    #endregion

    #region Unity Methods

    void Start()
    {
        colorPercentage = 0f;
        isGettingLighter = false;
        text = GetComponent<TextMeshProUGUI>();
        StartCoroutine(AnimateText());
    }

    #endregion

    #region Private Methods

    private IEnumerator AnimateText()
    {
        if(isGettingLighter)
        {
            colorPercentage -= COLOR_STEP;
            if(colorPercentage <= 0f)
            {
                isGettingLighter = false;
            }
        }
        else
        {
            colorPercentage += COLOR_STEP;
            if (colorPercentage >= 1f)
            {
                isGettingLighter = true;
            }
        }
        float fraction = colorPercentage / 0.5f;
        Color colorA = lighterColor;
        Color colorB = mainColor;
        if(colorPercentage >= 0.5f)
        {
            fraction -= 1f;
            colorA = mainColor;
            colorB = darkerColor;
        }

        text.color = Color.Lerp(colorA, colorB, fraction);
        yield return new WaitForSeconds(WAIT_PERIOD);
        StartCoroutine(AnimateText());
    }

    #endregion

}

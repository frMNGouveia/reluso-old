using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Localization;
using UnityEngine.Localization.Tables;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Collider2D))]
public class LoadingScreenBehavior : MonoBehaviour
{

    #region Internal Classes

    [RequireComponent(typeof(Collider2D))]
    private class LoadOnClick : MonoBehaviour
    {
        public AsyncOperation loadOp;
        public Canvas canvas;

        private void OnMouseDown()
        {
            float waitTime = FadePanel.FADE_WAIT;
            FadePanel panel = BetaGameUtil.InstantiateFadeOutPanel(canvas);
            waitTime += panel.waitBeforeFade;
            StartCoroutine(BetaGameUtil.WaitForFadeOutAndLoad(loadOp, waitTime));
        }

    }

    #endregion

    #region Constant Fields

    private const float LOADING_PROGRESS_UPDATE_WAIT = 0.1f;

    private const float FACT_REFRESH_TIME_INTERVAL = 20f;
    private const int FACT_BUFFER_SIZE = 5;

    #endregion

    #region Variable Fields

    //Public Static Variables
    public static float CurrentLoadProgress = 0.0f;

    //Public Variables
    public GameObject loadingView;
    public GameObject loadedView;
    public Slider loadingBar;
    
    public FactsDisplayText factsText;

    public LocalizedStringTable factsTable;

    //Private Variables
    private ICollection<StringTableEntry> currentTableValues;
    private int factBufferCounter;
    private int factBufferTotal;
    private string[] factBuffer;

    private AsyncOperation loadOp;

    private Canvas canvas;

    #endregion

    #region Unity Methods

    void Start()
    {
        loadedView.SetActive(false);
        loadingView.SetActive(true);
        StringTable currentFactsTable = factsTable.GetTable();
        currentTableValues = currentFactsTable.Values;

        factBufferCounter = 0;
        factBuffer = new string[FACT_BUFFER_SIZE];
        PopulateFactBuffer();

        canvas = GetComponentInParent<Canvas>();

        StartAsyncLoadScene();
        StartCoroutine(UpdateFactDisplay());
    }

    #endregion

    #region Private Methods

    private void StartAsyncLoadScene()
    {
        CurrentLoadProgress = 0.0f;
        loadingBar.value = 0.0f;
        loadOp = SceneManager.LoadSceneAsync(BetaGameUtil.indexOfSceneToLoad, LoadSceneMode.Single);
        loadOp.allowSceneActivation = false;
        StartCoroutine(UpdateLoadingScreen());
    }

    private IEnumerator UpdateLoadingScreen()
    {
        while (loadOp.isDone)
        {
            yield return new WaitForSeconds(LOADING_PROGRESS_UPDATE_WAIT);
            loadingBar.value = CurrentLoadProgress / 0.9f;
        }
        OnSceneLoaded();
    }

    private void OnSceneLoaded()
    {
        loadedView.SetActive(true);
        loadingView.SetActive(false);
        
        LoadOnClick load = this.gameObject.AddComponent<LoadOnClick>();
        load.loadOp = loadOp;
        load.canvas = this.canvas;
    }

    private IEnumerator UpdateFactDisplay()
    {
        factsText.DisplayFact(GetRandomFact());
        yield return new WaitForSeconds(FACT_REFRESH_TIME_INTERVAL);
        if(HasFactsLeftInBuffer())
            StartCoroutine(UpdateFactDisplay());
    }

    private bool HasFactsLeftInBuffer()
    {
        return factBufferCounter < factBufferTotal;
    }

    private void PopulateFactBuffer()
    {
        bool[] factAdded = new bool[currentTableValues.Count];
        int randomIndex;
        int i;
        for (i = 0; i < FACT_BUFFER_SIZE; i++)
        {
            randomIndex = Random.Range(0, currentTableValues.Count - 1);
            factAdded[randomIndex] = true;
        }

        factBufferTotal = 0;
        i = 0;
        foreach (StringTableEntry e in currentTableValues)
        {
            if (factAdded[i])
            {
                factBuffer[factBufferTotal++] = e.GetLocalizedString();
                if (factBufferTotal >= FACT_BUFFER_SIZE)
                {
                    break;
                }
            }
            i++;
        }
    }

    private string GetRandomFact()
    {
        return factBuffer[factBufferCounter++];
    }

    #endregion
    
}

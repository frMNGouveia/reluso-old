
[System.Serializable]
public class PlayerProgress
{

    #region Constant Fields

    #endregion

    #region Variable Fields

    public int score;

    #endregion

    #region Constructor Methods

    public PlayerProgress() : this(0)
    {

    }

    public PlayerProgress(int score)
    {
        this.score = score;
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    #endregion

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using static LanguageButton;
using static LanguageButton.LanguageOption;

public static class LanguagePersistence
{

    #region Constant Fields

    private const string LANGUAGE_PREF = "Language";
    private const int LANGUAGE_PREF_DEFAULT = (int) Pt;

    private static readonly LocaleIdentifier LOCALE_EN = new LocaleIdentifier(SystemLanguage.English);
    private static readonly LocaleIdentifier LOCALE_PT = new LocaleIdentifier(SystemLanguage.Portuguese);

    #endregion

    #region Variable Fields

    #endregion

    #region Public Methods


    public static void SaveLanguagePref(LanguageOption op)
    {
        PlayerPrefs.SetInt(LANGUAGE_PREF, (int)op);
    }

    public static LanguageOption LoadLanguagePref()
    {
        int lang = PlayerPrefs.GetInt(LANGUAGE_PREF, LANGUAGE_PREF_DEFAULT);
        return (LanguageOption)lang;
    }

    public static LanguageOption GetLanguageByLocale(Locale l)
    {
        if (l.Identifier.Equals(LOCALE_EN))
            return En;
        return Pt;
    }

    public static Locale FindLocaleWithIdentifier(LocaleIdentifier id)
    {
        foreach (Locale l in LocalizationSettings.AvailableLocales.Locales)
            if (l.Identifier.Equals(id))
                return l;
        return null;
    }

    public static Locale GetLocaleByLanguage(LanguageOption op)
    {
        switch (op)
        {
            case En:
                return FindLocaleWithIdentifier(LOCALE_EN);
            case Pt:
            default:
                return FindLocaleWithIdentifier(LOCALE_PT);
        }
    }

    public static bool HasLocales()
    {
        return LocalizationSettings.AvailableLocales.Locales.Count > 0;
    }

    #endregion

    #region Private Methods

    #endregion

}

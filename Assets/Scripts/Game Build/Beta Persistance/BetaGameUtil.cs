﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using static ScriptablesUtil;

public static class BetaGameUtil
{

    #region Enums

    public enum GameType
    {
        Menu,
        Levelled,
        Infinity
    }

    #endregion

    #region Constant/Read-Only Fields

    public const int SCENE_INDEX_MAIN_MENU = 0;
    public const int SCENE_INDEX_LOADING_SCREEN = 1;
    public const int SCENE_INDEX_TRASH_SEPARATOR = 2;

    public const string DEFAULT_DIFFICULTY_NAME = "IMedium";
    public const string DEFAULT_CONTAINER_CONFIG_NAME = "4Containers";

    public const string DEFAULT_GENERATOR_CONFIG_NAME = "PLACEHOLDER";
    public const string DEFAULT_SEPARATOR_CONFIG_NAME = "PLACEHOLDER";

    public static readonly FadePanel FADE_OUT_PREFAB = Resources.Load<FadePanel>(AssetUtil.FADE_OUT_PANEL_PREFAB);

    #endregion

    #region Variable Fields

    public static GameType currentGameType;
    public static SeparatorConfig currentSeparatorConfig = GetDefaultSeparatorConfig();

    public static bool isInDebugMode = Application.isEditor;
    
    public static int indexOfSceneToLoad;

    #endregion

    #region Public Methods

    public static AsyncOperation FadeAndLoadScene(int sceneIndex, Canvas canvas, MonoBehaviour context)
    {
        AsyncOperation op = LoadSceneScreen(sceneIndex);
        if (FADE_OUT_PREFAB != null && canvas != null)
        {
            op.allowSceneActivation = false;
            float waitTime = FadePanel.FADE_WAIT;
            FadePanel panel = InstantiateFadeOutPanel(canvas);
            waitTime += panel.waitBeforeFade;
            context.StartCoroutine(WaitForFadeOutAndLoad(op, waitTime));
        }
        return op;
    }

    public static AsyncOperation LoadSceneScreen(int sceneIndex)
    {
        Time.timeScale = 1.0f;
        indexOfSceneToLoad = sceneIndex;
        return SceneManager.LoadSceneAsync(SCENE_INDEX_LOADING_SCREEN);
    }

    public static void LoadGarbageSpearatorScene(SeparatorConfig separatorConfig)
    {
        currentSeparatorConfig = separatorConfig;
        LoadSceneScreen(SCENE_INDEX_TRASH_SEPARATOR);
    }

    public static FadePanel InstantiateFadeOutPanel(Canvas canvas)
    {
        return Object.Instantiate<FadePanel>(FADE_OUT_PREFAB, canvas.transform);
    }

    public static IEnumerator WaitForFadeOutAndLoad(AsyncOperation op, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        op.allowSceneActivation = true;
    }

    #endregion

}

﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using static LanguageButton;
using static LanguageButton.LanguageOption;

public static class PersistenceManager
{

    #region Constant Fields

    private static readonly string SAVE_PATH = Application.persistentDataPath + "/";
    private static readonly string SAVE_FILE_NAME = "progress.sav";
    private static readonly string SAVE_FILE_PATH = SAVE_PATH + SAVE_FILE_NAME;

    #endregion

    #region Variable Fields

    public static LanguageOption languagePref = LanguagePersistence.LoadLanguagePref();
    public static PlayerProgress progress = LoadPlayerProgress();

    #endregion

    #region Public Methods

    public static void SaveLanguagePref(LanguageOption op)
    {
        LanguagePersistence.SaveLanguagePref(op);
        languagePref = op;
    }

    public static void UpdateLanguageLocale()
    {
        LocalizationSettings.SelectedLocale = LanguagePersistence.GetLocaleByLanguage(languagePref);
    }

    public static void SavePlayerProgress()
    {
        SavePlayerProgress(progress);
    }

    #endregion

    #region Private Methods

    private static PlayerProgress LoadPlayerProgress()
    {
        PlayerProgress progress;
        if (File.Exists(SAVE_FILE_PATH))
        {
            FileStream outStream = null;
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                outStream = new FileStream(SAVE_FILE_PATH, FileMode.Open);

                progress = formatter.Deserialize(outStream) as PlayerProgress;
                outStream.Close();
            }
            catch (SerializationException e)
            {
                if(outStream != null)
                    outStream.Close();
                Debug.LogError("Player Progress File corrupted. Creating a new one.");
                Debug.LogError(e.StackTrace);
                progress = CreateNewPlayerProgress();
            }
        }
        else
            progress = CreateNewPlayerProgress();

        return progress;
    }



    private static void SavePlayerProgress(PlayerProgress progress)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream outStream = new FileStream(SAVE_FILE_PATH, FileMode.Create);

        formatter.Serialize(outStream, progress);
        outStream.Close();
    }

    private static PlayerProgress CreateNewPlayerProgress()
    {
        PlayerProgress progress = new PlayerProgress();
        SavePlayerProgress(progress);
        return progress;
    }

    #endregion

}

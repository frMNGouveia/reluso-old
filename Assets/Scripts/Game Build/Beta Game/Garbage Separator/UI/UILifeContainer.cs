using UnityEngine;
using UnityEngine.UI;
using static UILifeTally;

public class UILifeContainer : MonoBehaviour
{

    #region Variable Fields

    public GameObject rendererObject;

    private Image interiorImage;
    private UILifeMaterial material;

    #endregion

    #region Public Methods

    public void SetupContainer()
    {
        interiorImage = rendererObject.GetComponent<Image>();
        material = null;
    }

    public void SetMaterial(UILifeMaterial material)
    {
        this.material = material;

        if (interiorImage == null || material == null)
            return;
        if(material.Color == null || material.Material == null)
        {
            rendererObject.SetActive(false);
        }
        else
        {
            interiorImage.material = material.Material;
            rendererObject.SetActive(true);
        }
    }

    public Color? GetColor()
    {
        return material.Color;
    }

    #endregion
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIPowerupTimer : MonoBehaviour
{

    #region Variable Fields

    public Image image;
    public TextMeshProUGUI timer;
    public UIPowerupEventManager eventManager;

    private PowerupStatus status = null;

    #endregion

    #region Unity Methods

    void Start()
    {
        if (image == null)
            image = GetComponentInChildren<Image>();
        if (timer == null)
            timer = GetComponentInChildren<TextMeshProUGUI>();
        this.gameObject.SetActive(false);
    }

    void Update()
    {
        if (status == null)
            return;
        float timeLeft = status.deactivateAction.GetTimeLeft();
        if (status.active)
            timer.text = UIManager.ParseTime(timeLeft);
        else
            DisableTimer();
    }

    #endregion

    #region Public Methods

    public void SetStatus(PowerupStatus status)
    {
        this.status = status;
        if(status == null)
            this.gameObject.SetActive(false);
        else
        {
            image.sprite = status.powerup.sprite;
            this.gameObject.SetActive(true);
        }
    }

    public void SetupController(UIPowerupEventManager controller)
    {
        this.eventManager = controller;
    }

    #endregion

    #region Private Methods

    private void DisableTimer()
    {
        this.gameObject.SetActive(false);
        eventManager.OnPowerupTimerUpdate();
    }

    #endregion

}

using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;

public class UICountdown : MonoBehaviour
{

    public TextMeshProUGUI countdownText;

    [SerializeField]
    public LocalizedString countdownStart;

    private bool countingDown;

    private void Start()
    {
        countdownText.text = countdownStart.GetLocalizedString();
    }

    public void StartCountDown(int seconds, Action onCountDownOver)
    {
        if(!countingDown)
        {
            StartCoroutine(UpdateCountDownCoroutine(seconds, onCountDownOver));
        }
    }

    private IEnumerator UpdateCountDownCoroutine(int seconds, Action onCountDownOver)
    {
        countingDown = true;
        for(int countdown = seconds; countdown > 0; countdown--)
        {
            countdownText.text = "" + countdown;
            yield return new WaitForSeconds(1.0f);
        }
        this.gameObject.SetActive(false);
        countingDown = false;
        onCountDownOver();
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static AssetUtil;
using static MaterialsUtil;

public class UILifeTally : MonoBehaviour
{

    #region Internal Classes

    public class UILifeMaterial
    {
        public Material Material { get; set; }
        public Color? Color { get; set; }

        public UILifeMaterial(Material material, Color? color)
        {
            Material = material;
            Color = color;
            if(Material != null && Color != null)
            {
                Material.SetColor(LIFE_COLOR_PARAMETER, (Color)Color);
            }
        }

    }

    #endregion

    #region Constant Fields

    private const string LIFE_COLOR_PARAMETER = MAT_COLOR_RARITY;

    private const float PARTICLE_SCALE = 0.5f;
    private const float PARTICLE_DESTROY_WAIT = 1.1f;

    private static readonly Vector2 LIFE_TALLY_ANCHOR_MIN = new Vector2(1f, 0.5f);
    private static readonly Vector2 LIFE_TALLY_ANCHOR_MAX = LIFE_TALLY_ANCHOR_MIN;
    private static readonly Vector2 LIFE_TALLY_PIVOT = LIFE_TALLY_ANCHOR_MIN;

    private static readonly Vector3 LIFE_TALLY_DEFAULT_POS = Vector3.zero;

    #endregion

    #region Variable Fields

    public int numberOfContainers;
    public UILifeContainer heartPrefab;
    public ParticleSystem fadeOutPrefab;
    public ParticleSystem fadeInPrefab;

    private static UILifeMaterial[] lifeMaterials;
    private static Material heartMaterialPrefab;
    private static bool wasInitialized = false;
    private static bool hasErrors = false;
    
    private int gameLives;
    private int displayedLives;
    private IList<UILifeContainer> containers;

    private Canvas canvas;

    #endregion

    #region Unity Methods

    public void Start()
    {
        if(!wasInitialized)
        {
            lifeMaterials = InitializeMaterialsArray();
            wasInitialized = true;
        }

        containers = new List<UILifeContainer>(numberOfContainers);
        gameLives = displayedLives = 0;

        if (numberOfContainers == default || numberOfContainers < 0)
            numberOfContainers = Difficulty.DEFAULT_NUMBER_OF_LIVES;

        if (lifeMaterials != null && lifeMaterials.Length > 0)
        {
            UpdateContainers();
        }
        else
        {
            hasErrors = true;
            gameObject.SetActive(false);
        }
    }

    #endregion

    #region Static Methods

    private static UILifeMaterial[] InitializeMaterialsArray()
    {
        heartMaterialPrefab = Resources.Load<Material>(MAT_LIFE_PATH);
        Color[] lifeColors = Resources.Load<ColorCollection>(COLORS_LIFE_HEART_PATH).colorCollection;
        UILifeMaterial[] lifeMaterials = new UILifeMaterial[lifeColors.Length + 1];

        lifeMaterials[0] = new UILifeMaterial(null, null);

        for(int i = 1; i < lifeMaterials.Length; i++)
        {
            lifeMaterials[i] = new UILifeMaterial(new Material(heartMaterialPrefab), lifeColors[i - 1]);
        }

        return lifeMaterials;
    }

    #endregion

    #region Public Methods

    public void SetupLives(int lives)
    {
        if (hasErrors)
            return;

        gameLives = displayedLives = lives;

        int currentMaterial = GetCurrentMaterialIndex();
        int currentContainer = GetCurrentContainerIndex();

        UILifeMaterial mat1 = MaterialAtIndex(currentMaterial + 1);
        UILifeMaterial mat2 = MaterialAtIndex(currentMaterial);

        int i = 0;
        for(; i <= currentContainer; i++)
        {
            containers[i].SetMaterial(mat1);
        }
        for (; i < containers.Count; i++)
        {
            containers[i].SetMaterial(mat2);
        }
    }

    public void UpdateLives(int lives)
    {
        if (!gameObject.activeSelf)
            return;

        gameLives = lives;
        if (containers.Count != numberOfContainers)
        {
            UpdateContainers();
        }
        AnimateLifeUpdate();
    }

    #endregion

    #region Private Methods

    private int GetCurrentMaterialIndex()
    {
        return (displayedLives - 1) / containers.Count;
    }

    private int GetCurrentContainerIndex()
    {
        return (displayedLives - 1) % containers.Count;
    }

    private void AnimateLifeUpdate()
    {
        if (gameLives == displayedLives || displayedLives <= 0)
            return;

        if (displayedLives < gameLives)
        {
            IncreaseLife();
        }
        else
        {
            DecreaseLife();
        }
        AnimateLifeUpdate();
    }

    private void IncreaseLife()
    {
        displayedLives++;
        int index = GetCurrentContainerIndex();
        UILifeMaterial mat = MaterialAtIndex(GetCurrentMaterialIndex() + 1);

        SpawnParticles(containers[index], (Color) mat.Color, fadeInPrefab);
        containers[index].SetMaterial(mat);
    }

    private void DecreaseLife()
    {
        int index = GetCurrentContainerIndex();
        UILifeMaterial mat = MaterialAtIndex(GetCurrentMaterialIndex());
        displayedLives--;

        Color? c = containers[index].GetColor();
        GameObject particles = SpawnParticles(containers[index], (Color) c, fadeOutPrefab);
        StartCoroutine(WaitAndDestroyParticles(particles, PARTICLE_DESTROY_WAIT));
        containers[index].SetMaterial(mat);
    }

    private IEnumerator WaitAndDestroyParticles(GameObject particles, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        if (particles != null)
            Destroy(particles);
    }

    private void UpdateContainers()
    {
        if (numberOfContainers > containers.Count)
        {
            for (int i = containers.Count; i < numberOfContainers; i++)
            {
                containers.Add(CreateNewHeartContainer());
            }
        }
        else if (numberOfContainers < containers.Count)
        {
            for (int i = containers.Count; i > numberOfContainers; i--)
            {
                DestroyContainerAt(i);
            }
        }
    }

    private GameObject SpawnParticles(UILifeContainer container, Color c, ParticleSystem particlePrefab)
    {
        RectTransform rectTransform = (RectTransform)container.transform;
        ParticleSystem particleSystem = Instantiate(particlePrefab, container.transform);
        GameObject particles = particleSystem.gameObject;

        particles.transform.localPosition = rectTransform.rect.center;
        particles.transform.localScale = particles.transform.localScale * PARTICLE_SCALE;

        var main = particleSystem.main;
        main.startColor = c;

        return particles;
    }

    private UILifeContainer CreateNewHeartContainer()
    {
        Vector3 pos;
        UILifeContainer container = Instantiate(heartPrefab, this.gameObject.transform);
        container.SetupContainer();
        RectTransform transform = container.gameObject.GetComponent<RectTransform>();

        if (containers.Count == 0)
        {
            pos = LIFE_TALLY_DEFAULT_POS;
        }
        else
        {
            float width = transform.rect.width;
            pos = new Vector3(-(width * containers.Count), 0f, 0f);
        }

        transform.anchorMin = LIFE_TALLY_ANCHOR_MIN;
        transform.anchorMax = LIFE_TALLY_ANCHOR_MAX;
        transform.pivot = LIFE_TALLY_PIVOT;
        container.gameObject.transform.localPosition = pos;

        return container;
    }

    private void DestroyContainerAt(int index)
    {
        GameObject heart = containers[index].gameObject;
        containers.RemoveAt(index);
        Destroy(heart);
    }

    private UILifeMaterial MaterialAtIndex(int index)
    {
        if (index < 0)
            return lifeMaterials[0];
        if (index >= lifeMaterials.Length)
            return lifeMaterials[lifeMaterials.Length - 1];
        else
            return lifeMaterials[index];
    }

    #endregion

}

using UnityEngine;
using TMPro;
using UnityEngine.Localization;
using System.Collections.Generic;
using UnityEngine.Localization.Settings;
using System.Collections;
using System;

public class UIManager : MonoBehaviour
{

    #region Constant Fields
    
    private const string LIVES_SEPARATOR_TEXT = "/";
    private const string TIME_SEPARATOR_TEXT = ":";

    private const string FINAL_SCORE_TEXT = "Final Score: ";
    private const string FINAL_TIME_TEXT = "Final Time: ";

    private const int COUNTDOWN_SECONDS = 3;

    #endregion

    #region Variable Fields

    public TextMeshProUGUI scoreText, timeText;
    public TextMeshProUGUI finalScoreText, finalTimeText;

    public UILifeTally lifeTally;

    public GameObject scoreUI;
    public GameObject endScreenInfini;
    public GameObject endScreenLevel;
    public GameObject pauseScreen;

    private GameObject endScreenScreen;

    public DialogBox dialog;

    [SerializeField]
    private LocalizedDialogData exitDialogData;
    [SerializeField]
    private LocalizedDialogData retryDialogData;

    public UIPowerupTally powerupTally;

    public FadePanel fadeInPanel;

    public UICountdown uiCountdown;

    #endregion

    #region Unity Methods

    void Start()
    {
        ActivateScreen(scoreUI);
        dialog.gameObject.SetActive(false);
        //TODO: Remove this
        endScreenScreen = endScreenInfini;
    }

    #endregion

    #region Public Methods

    public void SetupEndScreen()
    {
        //TODO: This
    }

    public void HideScore()
    {
        if (scoreUI != null)
            scoreUI.SetActive(false);
    }

    public void UpdateEndScreen(int successes, float time)
    {
        string finalScore = FINAL_SCORE_TEXT + successes;
        finalScoreText.text = finalScore;

        string finalTime = FINAL_TIME_TEXT + ParseTime(time);
        finalTimeText.text = finalTime;

    }

    public void DisplayPauseScreen()
    {
        ActivateScreen(pauseScreen);
    }
    
    public void DisplayGameScreen()
    {
        ActivateScreen(scoreUI);
    }

    public void DisplayEndScreen()
    {
        ActivateScreen(endScreenScreen);
    }

    public void SetupLives(int lives)
    {
        lifeTally.SetupLives(lives);
    }

    public void UpdateLives(int lives)
    {
        if (lifeTally == null)
            return;
        lifeTally.UpdateLives(lives);
    }

    public void UpdateScore(int score)
    {
        scoreText.text = "" + score;
    }

    public void DisplayExitGameDialog(Action onPosClick)
    {
        SetupDialog(onPosClick);
        dialog.disableOnPositiveClick = false;
        dialog.DisplayDialog(exitDialogData);
    }

    public void DisplayRetryGameDialog(Action onPosClick)
    {
        SetupDialog(onPosClick);
        dialog.DisplayDialog(retryDialogData);
    }

    public void UpdateTimer(IGarbageSeparator separator)
    {
        timeText.text = ParseTime(separator.GetClockCount());
    }

    public void SetupPowerupTimers(UIPowerupEventManager controller)
    {
        powerupTally.SetupPowerUpTally(controller);
    }

    public void UpdatePowerupTimers(IGarbageSeparator separator)
    {
        powerupTally.UpdatePowerupTimers(separator);
    }

    public IEnumerator WaitForFadeThenStartCountdown(Action onCountDownStart)
    {
        yield return new WaitForSeconds(FadePanel.FADE_WAIT + fadeInPanel.waitBeforeFade);
        onCountDownStart();
    }

    public void StartCountDown(Action onCountDownOver)
    {
        uiCountdown.gameObject.SetActive(true);
        uiCountdown.StartCountDown(COUNTDOWN_SECONDS, onCountDownOver);
    }

    public void DisplayCountdown()
    {
        uiCountdown.gameObject.SetActive(true);
    }

    #endregion

    #region Private Methods

    public static string ParseTime(float time)
    {
        int minutes = (int)(time / 60);
        int seconds = (int)(time % 60);
        string timeText = minutes + TIME_SEPARATOR_TEXT;
        if (seconds < 10)
            timeText += "0";
        timeText += seconds;
        return timeText;
    }

    private void ActivateScreen(GameObject screen)
    {
        if(endScreenScreen != null)
            endScreenScreen.SetActive(false);
        pauseScreen.SetActive(false);
        screen.SetActive(true);
    }
    
    private void SetupDialog(Action onPosClick)
    {
        dialog.onPositiveButtonClick = onPosClick;
        dialog.onNegativeButtonClick = DialogBox.DO_NOTHING;
        dialog.disableOnPositiveClick = true;
        dialog.disableOnNegativeClick = true;
    }

    #endregion
}

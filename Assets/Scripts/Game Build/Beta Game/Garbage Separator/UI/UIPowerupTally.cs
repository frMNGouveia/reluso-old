using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPowerupTally : MonoBehaviour
{

    #region Variable Fields

    public int maxPowerupTimers;
    public UIPowerupTimer powerupTimerPrefab;

    private UIPowerupEventManager powerupEvenManager;
    private List<UIPowerupTimer> timers;

    #endregion

    #region Public Methods

    public void SetupPowerUpTally(UIPowerupEventManager powerupEvenManager)
    {
        this.powerupEvenManager = powerupEvenManager;

        timers = new List<UIPowerupTimer>();
        for (int i = 0; i < maxPowerupTimers; i++)
            timers.Add(CreateNewPowerupTimer());
    }

    public void UpdatePowerupTimers(IGarbageSeparator separator)
    {
        List<PowerupStatus> statuses = new List<PowerupStatus>();
        ICollection<PowerupStatus> statusColl = separator.GetTemporaryPowerupStatuses();
        foreach (PowerupStatus s in statusColl)
        {
            if (s.active)
                statuses.Add(s);
            if (statuses.Count >= maxPowerupTimers)
                break;
        }

        statuses.Sort(CompareStatusesByTimeLeft);

        int i = 0;
        foreach (PowerupStatus s in statuses)
        {
            if (i >= maxPowerupTimers)
                break;
            timers[i++].SetStatus(s);
        }
        for (; i < maxPowerupTimers; i++)
            timers[i].SetStatus(null);
    }

    #endregion

    #region Private Methods

    private UIPowerupTimer CreateNewPowerupTimer()
    {
        UIPowerupTimer timer = Instantiate(powerupTimerPrefab, gameObject.transform);
        GameObject timerObj = timer.gameObject;

        RectTransform transform = timerObj.GetComponent<RectTransform>();
        float height = transform.rect.height * transform.localScale.y;
        transform.localPosition -= new Vector3(0f, height * timers.Count, 0f);
        
        timer.SetupController(powerupEvenManager);
        return timer;
    }

    private int CompareStatusesByTimeLeft(PowerupStatus s1, PowerupStatus s2)
    {
        if (s1 == s2)
            return 0;
        if (s2 == null)
            return -1;
        if (s1 == null)
            return 1;
        float diff = s1.deactivateAction.GetTimeLeft() - s2.deactivateAction.GetTimeLeft();
        return (int)diff;
    }

    #endregion

}

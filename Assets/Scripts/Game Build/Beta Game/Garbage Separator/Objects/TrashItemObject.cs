﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class TrashItemObject : ItemObject
{
    #region Constant Fields

    protected const float SELF_DELETE_DELAY = 0.2f;

    #endregion

    #region Variable Fields

    protected Dictionary<int, ContainerCollider> colliders = new Dictionary<int, ContainerCollider>();

    protected bool depositing;
    protected int coroutineCall = 0;

    #endregion

    #region Unity Methods

    private void Start()
    {
        coroutineCall = 0;
    }

    #endregion

    #region Public Methods

    public virtual void DepositInContainer(ContainerCollider collider)
    {
        if(!colliders.ContainsKey(collider.id))
        {
            colliders.Add(collider.id, collider);
            if(!depositing)
            {
                StartCoroutine(WaitAndDeposit(SELF_DELETE_DELAY));
            }
        }
    }

    public virtual void LeaveContainer(ContainerCollider collider)
    {
        colliders.Remove(collider.id);
        if(colliders.Count <= 0)
        {
            depositing = false;
        }
    }

    #endregion

    #region Protected Methods

    protected IEnumerator WaitAndDeposit(float waitTime)
    {
        depositing = true;
        if (coroutineCall <= 1)
        {
            coroutineCall++;
            yield return new WaitForSeconds(waitTime);

            ContainerCollider closestContainer = null;
            float closestDistance = float.MaxValue;
            float distance;

            foreach (ContainerCollider c in colliders.Values)
            {
                distance = Vector2.Distance(c.transform.position, this.transform.position);
                if (distance < closestDistance)
                {
                    closestContainer = c;
                    closestDistance = distance;
                }

            }

            if (closestContainer != null)
                closestContainer.GetContainer().DepositItem(this);
        }
        depositing = false;
    }

    #endregion

}

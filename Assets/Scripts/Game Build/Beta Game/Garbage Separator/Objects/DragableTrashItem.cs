﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using System.Collections;
using UnityEngine;

public class DragableTrashItem : TrashItemObject
{
    #region Constant Fields

    protected const float REPEAT_DELETE_DELAY = 0.1f;

    #endregion

    #region Variable Fields

    private Vector3 screenPoint;
    private Vector3 offset;

    private bool isBeingDragged = false;

    private Rigidbody2D rigidBody;

    #endregion

    #region Unity Methods

    public void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
    }


    void OnMouseDown()
    {
        isBeingDragged = true;

        if (rigidBody != null)
            rigidBody.bodyType = RigidbodyType2D.Kinematic;

        //Segment of code gotten online
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

    }

    void OnMouseDrag()
    {
        //Segment of code gotten online
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
    }

    void OnMouseUp()
    {
        isBeingDragged = false;
        if (rigidBody != null)
        {
            rigidBody.bodyType = RigidbodyType2D.Dynamic;
            rigidBody.velocity = Vector2.zero;
        }
    }

    #endregion

    #region Public Methods

    public bool IsBeingDragged()
    {
        return isBeingDragged;
    }

    public override void DepositInContainer(ContainerCollider collider)
    {
        if (!colliders.ContainsKey(collider.id))
        {
            colliders.Add(collider.id, collider);
            if (!depositing)
            {
                StartCoroutine(WaitIfDragged(collider));
            }
        }
    }

    #endregion

    #region Private Methods

    private IEnumerator WaitIfDragged(ContainerCollider collider)
    {
        if (!isBeingDragged)
            StartCoroutine(WaitAndDeposit(SELF_DELETE_DELAY));
        else
        {
            yield return new WaitForSeconds(REPEAT_DELETE_DELAY);

            StartCoroutine(WaitIfDragged(collider));
        }
    }

    #endregion

}

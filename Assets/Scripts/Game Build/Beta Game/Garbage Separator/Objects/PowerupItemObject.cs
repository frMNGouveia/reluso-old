using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupItemObject : ItemObject
{

    #region Unity Methods

    void OnMouseDown()
    {
        controller.ActivatePowerup(this);
    }

    #endregion

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemObject : MonoBehaviour
{

    #region Static Fields

    public static GarbageSeparatorController controller;

    #endregion

    #region Variable Fields

    public int id;
    public Item item;
    public bool triggeredSpawn = false;
    public SpriteRenderer spriteRenderer;

    #endregion

}

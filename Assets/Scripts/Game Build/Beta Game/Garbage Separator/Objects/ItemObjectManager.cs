using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemObjectManager : MonoBehaviour
{

    #region Variable Fields

    private Dictionary<int, ItemObject> itemObjects;
    private int itemCounter;

    #endregion

    #region Unity Methods

    public void Start()
    {
        ResetItems();
    }

    #endregion

    #region Public Methods

    public void ResetManager()
    {
        ClearAllObjects();
        ResetItems();
    }

    public int GenerateNewID()
    {
        return itemCounter++;
    }

    public void AddObject(int id, ItemObject itemObject)
    {
        itemObjects.Add(id, itemObject);
    }

    public void DestroyObjectWithID(int id)
    {
        if (!itemObjects.ContainsKey(id))
            return;
        ItemObject obj = itemObjects[id];
        itemObjects.Remove(id);
        if(obj != null)
            Destroy(obj.gameObject);
    }

    public void ClearAllObjects()
    {
        foreach (KeyValuePair<int, ItemObject> p in itemObjects)
            Destroy(p.Value);
        
        foreach (ItemObject obj in this.gameObject.GetComponentsInChildren<ItemObject>(true))
            Destroy(obj.gameObject);
    }

    public ICollection<ItemObject> GetAllObjects()
    {
        return itemObjects.Values;
    }

    #endregion

    #region Public Methods

    private void ResetItems()
    {
        itemObjects = new Dictionary<int, ItemObject>();
        itemCounter = 0;
    }

    #endregion
}

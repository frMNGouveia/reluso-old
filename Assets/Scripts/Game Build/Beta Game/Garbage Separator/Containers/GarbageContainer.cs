﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using UnityEngine;
using System.Collections;
using static MaterialsUtil;

public class GarbageContainer : MonoBehaviour
{

    #region Constant Fields

    private const string CONTAINER_COLOR_PARAMETER = MAT_COLOR_MULT;

    #endregion

    #region Variable Fields

    public static GarbageSeparatorController controller;
    public GameObject containerSprites;

    private Container container;
    private SpriteRenderer[] renderers;

    #endregion

    #region Unity Methods

    void Start()
    {
        SetupController();
        SetupSpriteRenderers();
    }

    #endregion

    #region Public Methods

    public Container GetContainer()
    {
        return container;
    }

    public void SetContainer(Container container)
    {
        if (renderers == null)
            SetupSpriteRenderers();

        this.container = container;
        if(renderers.Length > 0)
        {
            Material containerMaterial = new Material(renderers[0].material);
            foreach (Renderer r in renderers)
                r.material = containerMaterial;
            containerMaterial.SetColor(CONTAINER_COLOR_PARAMETER, GetContainerColor());
        }
    }

    public void DepositItem(TrashItemObject obj)
    {
        StartCoroutine(DepositAfterTime(GarbageSeparatorController.DESTROYER_DELAY, obj));
    }

    #endregion

    #region Private Methods

    private IEnumerator DepositAfterTime(float delay, TrashItemObject obj)
    {
        yield return new WaitForSeconds(delay);

        if (controller == null)
            SetupController();

        controller.DepositGarbageObject(obj, this);
    }

    private Color GetContainerColor()
    {
        return container.color;
    }

    private void SetupController()
    {
        controller = gameObject.GetComponentInParent<GarbageSeparatorController>();
    }

    private void SetupSpriteRenderers()
    {
        renderers = containerSprites.GetComponentsInChildren<SpriteRenderer>();
    }

    #endregion
	
}

﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using System.Collections;
using UnityEngine;

public class ContainerCollider : MonoBehaviour
{

    #region Variable Fields

    public int id;

    private GarbageContainer container;

    #endregion

    #region Unity Methods

    void Start()
    {
        container = GetComponentInParent<GarbageContainer>();
    }

    #endregion

    #region Public Methods

    public GarbageContainer GetContainer()
    {
        return container;
    }

    #endregion

    #region Private Methods

    private void OnTriggerEnter2D(Collider2D other)
    {
        TrashItemObject obj = other.gameObject.GetComponent<TrashItemObject>();
        if (obj != null)
        {
            obj.DepositInContainer(this);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        TrashItemObject obj = other.gameObject.GetComponent<TrashItemObject>();
        if (obj != null)
        {
            obj.LeaveContainer(this);
        }
    }

    #endregion

}

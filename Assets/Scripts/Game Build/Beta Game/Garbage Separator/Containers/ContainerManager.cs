﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using UnityEngine;

public class ContainerManager : MonoBehaviour
{

    #region Constant Fields

    private const string CONTAINER_BASE_NAME = "Container ";
    private const string CONTAINER_COL_BASE_NAME = " Containers";

    private const float CONTAINER_COL_Y = 0.4f;
    private const float CONTAINER_COL_SCALE_X = 0.9f;
    private const float CONTAINER_COL_SCALE_Y = 0.7f;

    private const float REGULAR_CONTAINER_WIDTH = 5.2f;

    private const float WORLD_WIDTH = 25f;
    private const float WORLD_DEVIATION = WORLD_WIDTH / 2.0f;

    #endregion

    #region Variable Fields

    public ContainerCollection[] existingCollections;
    public GameObject containerPrefab;

    private ContainerCollection collection;
    private GarbageSeparatorController controller;

    #endregion

    #region Public Methods

    public void SetupContainers(Container[] configContainers)
    {
        foreach (ContainerCollection col in existingCollections)
            col.gameObject.SetActive(false);

        //Check if there is an existing display for the respective ammount of containers, if not, create one
        if (configContainers.Length <= existingCollections.Length)
            collection = SetupContainerCollection(configContainers);
        else
            collection = CreateContainerCollection(configContainers);

        //If collection is null, something went wrong
        if (collection == null)
        {
            Debug.LogError("Could not find or create a Container Collection.");
            return;
        }

        //Setup each of the containers to their respective configurations
        GarbageContainer[] containers = collection.GetContainers();
        for (int i = 0; i < containers.Length && i < configContainers.Length; i++)
            containers[i].SetContainer(configContainers[i]);
    }

    #endregion
 
    #region Private Methods

    private ContainerCollection SetupContainerCollection(Container[] configContainers)
    {
        ContainerCollection col = existingCollections[configContainers.Length - 1];
        col.gameObject.SetActive(true);
        return col;
    }

    private ContainerCollection CreateContainerCollection(Container[] configContainers)
    {
        //Create the Container Collection Object (display)
        GameObject colObj = new GameObject();
        colObj.SetActive(false);

        //Set the Container Collection Object name according to used nomenclature
        colObj.name = configContainers.Length + CONTAINER_COL_BASE_NAME;

        //Position the Container Collection according to standard
        Transform trans = colObj.GetComponent<Transform>();
        if(trans == null)
            trans = colObj.AddComponent<Transform>();

        //Position and scale Container Collection
        trans.parent = gameObject.transform;
        trans.localScale = new Vector3(CONTAINER_COL_SCALE_X, CONTAINER_COL_SCALE_Y, 1);
        trans.localPosition = new Vector3(0, CONTAINER_COL_Y, 0);

        //Adds Container Collection Behaviour class to object
        ContainerCollection col = colObj.AddComponent<ContainerCollection>();

        //Determine scale for each of the containers
        float container_width = WORLD_WIDTH / (configContainers.Length + 1);
        float container_x_scale = container_width / REGULAR_CONTAINER_WIDTH;

        GameObject obj;
        ContainerCollider collider;
        float newX = -(WORLD_DEVIATION) + (container_width / 2) ;

        //Create each of the new containers and add them to the Container Collection
        for(int i = 0; i < configContainers.Length; i++)
        {
            obj = Instantiate(containerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            obj.SetActive(false);

            //Set Container Collection as parent of the container
            obj.transform.parent = colObj.transform;

            //Set the name of the Container accodring to nomenclature
            obj.name = CONTAINER_BASE_NAME + configContainers.Length + "." + (i+1);

            //Set the scale and location of the containers
            obj.transform.localScale = new Vector3(container_x_scale, 1, 1);
            obj.transform.localPosition = new Vector3(newX, 0, 0);

            collider = obj.GetComponentInChildren<ContainerCollider>();
            if (collider != null)
                collider.id = i;

            obj.SetActive(true);

            //Determine the position of the next container
            newX += container_width;
        }

        colObj.SetActive(true);
        
        return col;
    } 

    #endregion
	
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainerCollection : MonoBehaviour
{

    private GarbageContainer[] containers;
    
    public GarbageContainer[] GetContainers()
    {
        if (containers == null)
            containers = GetComponentsInChildren<GarbageContainer>();
        return containers;
    }

}

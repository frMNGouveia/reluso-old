﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
*/
using System.Collections;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{

    #region Variable Fields

    public GarbageSeparatorController controller;

    private bool isWithinTrigger, isSpawning, spawnerActive;

    #endregion

    #region Unity Methods

    void Start()
    {
        isWithinTrigger = true;
        isSpawning = false;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!spawnerActive)
            return;
        ItemObject item = other.gameObject.GetComponent<ItemObject>();

        if (item == null || item.triggeredSpawn)
            return;

        item.triggeredSpawn = true;
        isWithinTrigger = false;
        if (!isWithinTrigger && !isSpawning)
            StartCoroutine(SpawnAfterTime(0f, other.gameObject));
    }

    public void ToggleSpawner(bool active)
    {
        spawnerActive = active;
    }

    #endregion

    #region Private Methods

    private IEnumerator SpawnAfterTime(float delay, GameObject obj)
    {
        isSpawning = true;
        yield return new WaitForSeconds(delay);

        if (spawnerActive)
        {
            controller.SpawnNewItemObject();
            isWithinTrigger = true;
        }
        isSpawning = false;
    }

    #endregion

}

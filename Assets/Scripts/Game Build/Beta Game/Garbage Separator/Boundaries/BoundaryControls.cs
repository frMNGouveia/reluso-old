﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
*/
using System.Collections;
using UnityEngine;

public class BoundaryControls : MonoBehaviour
{

    #region Variable Fields

    private const float WAIT_TIME = 0.7f;

    #endregion

    #region Variable Fields

    public static GarbageSeparatorController controller;

    #endregion

    #region Unity Methods

    void Start()
    {
        controller = gameObject.GetComponentInParent<GarbageSeparatorController>();
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        ItemObject itemObject = other.gameObject.GetComponent<ItemObject>();
        if (itemObject != null)
            StartCoroutine(WaitForOutOfBounds(itemObject));
    }
    
    private IEnumerator WaitForOutOfBounds(ItemObject itemObject)
    {
        yield return new WaitForSeconds(WAIT_TIME);
        if(itemObject != null)
            controller.ObjectOutOfBounds(itemObject);
    }


    #endregion

}

using System.Collections.Generic;
using UnityEngine;

public class ItemSpawnerController : MonoBehaviour
{

    #region Constant Fields

    public const float SIZE_SCALER = 1.1f;

    #endregion

    #region Variable Fields

    public GameObject trashPrefab;
    public GameObject powerupPrefab;

    private ItemSpawner objectSpawner;
    private ItemObjectManager itemManager;
    private IGarbageSeparator trashSeparator;

    #endregion

    #region Public Methods

    public void SetupController(ItemObjectManager itemManager, ItemSpawner objectSpawner, IGarbageSeparator trashSeparator)
    {

        this.objectSpawner = objectSpawner;
        this.itemManager = itemManager;
        this.trashSeparator = trashSeparator;

    }

    public void SpawnNewItemObject()
    {
        Item newItem = trashSeparator.GenerateItem();
        int id = itemManager.GenerateNewID();
        ItemObject itemObject = CreateNewGameObject(newItem, id);
        itemManager.AddObject(id, itemObject);
    }

    public void RemoveObjectWithID(int id)
    {
        itemManager.DestroyObjectWithID(id);
    }

    public void EndGame()
    {
        objectSpawner.ToggleSpawner(false);
        itemManager.ClearAllObjects();
    }

    public void ResetController()
    {
        itemManager.ResetManager();
    }

    public void ActivateSpawner()
    {
        objectSpawner.ToggleSpawner(true);
    }

    public ICollection<ItemObject> GetAllActiveObjects()
    {
        return itemManager.GetAllObjects();
    }

    #endregion

    #region Private Methods

    private ItemObject CreateNewGameObject(Item newItem, int id)
    {
        ItemObject newObject = null;
        if (newItem is TrashItem)
            newObject = CreateNewTrashObject((TrashItem)newItem, id);
        else if (newItem is PowerupItem)
            newObject = CreateNewPowerupObject((PowerupItem)newItem, id);
        if (newObject != null)
        {
            //Make group the parent of the object
            Transform trans = newObject.GetComponent<Transform>();
            trans.SetParent(itemManager.transform);
            trans.localScale = new Vector3(3, 3, 1);
        }
        return newObject;
    }

    private string ObjectName(Item newItem, int id)
    {
        //TODO: Change this concatenation
        string name = "Item_" + id;
        if (newItem is TrashItem)
            name += "_Trash";
        else if (newItem is PowerupItem)
            name += "_Powerup";
        return name;
    }

    private ItemObject CreateNewTrashObject(TrashItem newItem, int id)
    {
        GameObject newObject = CreateNewItemObject(trashPrefab, newItem, id);

        //Add Script
        TrashItemObject item;
        item = newObject.GetComponent<DragableTrashItem>();
        item.id = id;
        item.item = newItem;

        newObject.SetActive(true);
        return item;
    }

    private ItemObject CreateNewPowerupObject(PowerupItem newItem, int id)
    {
        GameObject newObject = CreateNewItemObject(powerupPrefab, newItem, id);

        //Add Script
        PowerupItemObject item;
        item = newObject.GetComponent<PowerupItemObject>();
        item.id = id;
        item.item = newItem;

        newObject.SetActive(true);
        return item;
    }

    private GameObject CreateNewItemObject(GameObject prefab, Item newItem, int id)
    {
        GameObject newObject = Instantiate(prefab,
            objectSpawner.GetComponent<Transform>().position, Quaternion.identity);
        newObject.SetActive(false);

        newObject.name = ObjectName(newItem, id);


        //Render the object
        SpriteRenderer renderer = newObject.GetComponent<ItemObject>().spriteRenderer;
        if (renderer == null)
            renderer = newObject.GetComponent<SpriteRenderer>();
        if (newItem.sprite != null)
            renderer.sprite = newItem.sprite;

        //Adjust Collider Scale
        BoxCollider2D collider = newObject.GetComponent<BoxCollider2D>();
        collider.size = renderer.sprite.bounds.size * SIZE_SCALER;

        return newObject;
    }

    #endregion

}

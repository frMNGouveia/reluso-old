﻿/**
    * Created By: Francisco Miguel Nunes Gouveia
    * Created On: 
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GarbageSeparator;

public class GarbageSeparatorController : MonoBehaviour, ISeparatorStateManager, UIPowerupEventManager
{

    #region Constant Fields

    private const int TRASH_ITEM_SORTING_ORDER = 9;
    private const string TRASH_ITEM_SORTING_LAYER = "Over Conveyor";
    public const float DESTROYER_DELAY = 0f;

    private static readonly Vector3 MOVEMENT_DIRECTION = Vector3.left;

    #endregion

    #region Variable Fields

    //Public Fields
    public float conveyorSpeed;
    public float itemSpeed;

    public GameObject conveyor;

    public UIManager uiManager;
    public SeparatorConfig debugConfig;

    //Private Fields

    private Animator conveyorAnimator;

    private SeparatorConfig minigameConfig;
    private ItemSpawnerController itemSpawner;

    private IGarbageSeparator trashSeparator;

    private Canvas canvas;

    private float timeScale;

    #endregion

    #region Unity Methods

    void Awake()
    {
        SetupGame();
        uiManager.DisplayCountdown();
    }

    void Start()
    {
        canvas = uiManager.GetComponent<Canvas>();
        conveyorAnimator = conveyor.GetComponentInChildren<Animator>();
        HideDisplay();
        StartCoroutine(uiManager.WaitForFadeThenStartCountdown(CountdownThenStart));
    }
 
    void Update()
    {
        if (trashSeparator == null)
            return;
        UpdateGame();
    }

    #endregion

    #region Public Methods

    public void OnSeparatorStateChange(SeparatorState newState)
    {
        switch (newState)
        {
            case SeparatorState.Initial:
                ResetGame();
                break;
            case SeparatorState.Setup:
                StartGame();
                break;
            case SeparatorState.Generating:
                DisplayGame();
                break;
            case SeparatorState.Paused:
                PauseGame();
                break;
            case SeparatorState.Ended:
                EndGame();
                break;
        }
    }

    public void SpawnNewItemObject()
    {
        itemSpawner.SpawnNewItemObject();
    }

    public void DepositGarbageObject(TrashItemObject itemObj, GarbageContainer container)
    {
        if (itemObj == null)
            return;
        trashSeparator.PlaceTrashInContainer((TrashItem)itemObj.item, container.GetContainer());
        itemSpawner.RemoveObjectWithID(itemObj.id);
        UpdateStatistics();
    }

    public void ObjectOutOfBounds(ItemObject item)
    {
        if (item == null)
            return;

        if(item.item is TrashItem)
        {
            TrashItemObject trash = item.gameObject.GetComponent<TrashItemObject>();
            if (trash == null)
                return;
            trashSeparator.TrashOutOfBounds((TrashItem)trash.item);
            UpdateStatistics();
        }
        itemSpawner.RemoveObjectWithID(item.id);
    }

    public void GoToMainMenu()
    {
        BetaGameUtil.FadeAndLoadScene(BetaGameUtil.SCENE_INDEX_MAIN_MENU, canvas, uiManager);
    }

    public void Retry()
    {
        if (trashSeparator != null && trashSeparator.GetSeparatorState() == SeparatorState.Paused)
        {
            uiManager.DisplayRetryGameDialog(EndAndRestart);
        }
        else
        {
            CountdownThenRestart();
        }
    }

    public void Pause()
    {
        trashSeparator.Pause();
    }

    public void Unpause()
    {
        trashSeparator.Unpause();
    }

    public void Quit()
    {
        uiManager.DisplayExitGameDialog(GoToMainMenu);
    }

    public void ActivatePowerup(PowerupItemObject powerupObj)
    {
        if (powerupObj == null)
            return;

        trashSeparator.ActivatePowerup((PowerupItem) powerupObj.item);
        itemSpawner.RemoveObjectWithID(powerupObj.id);
        uiManager.UpdatePowerupTimers(trashSeparator);
    }

    public void OnPowerupTimerUpdate()
    {
        uiManager.UpdatePowerupTimers(trashSeparator);
    }

    #endregion

    #region Private Methods

    private void HideDisplay()
    {
        conveyorAnimator.speed = 0.0f;
        uiManager.HideScore();
    }

    private void CountdownThenStart()
    {
        uiManager.StartCountDown( () => OnSeparatorStateChange(SeparatorState.Initial));
    }

    private void CountdownThenRestart()
    {
        DisplayGame();
        HideDisplay();
        uiManager.StartCountDown(ResetGame);
    }

    private void EndAndRestart()
    {
        EndGame();
        CountdownThenRestart();
    }

    private void SetupGame()
    {
        LoadingScreenBehavior.CurrentLoadProgress = 0.1f;
        timeScale = Time.timeScale;

        if (debugConfig != null)
            minigameConfig = debugConfig;
        else if (BetaGameUtil.currentSeparatorConfig != null)
            minigameConfig = BetaGameUtil.currentSeparatorConfig;

        LoadingScreenBehavior.CurrentLoadProgress = 0.2f;

        ItemObjectManager itemManager = gameObject.GetComponentInChildren<ItemObjectManager>();
        ItemSpawner objectSpawner = gameObject.GetComponentInChildren<ItemSpawner>();
        itemSpawner = gameObject.GetComponentInChildren<ItemSpawnerController>();

        LoadingScreenBehavior.CurrentLoadProgress = 0.3f;

        ContainerManager manager = gameObject.GetComponentInChildren<ContainerManager>();
        if (manager != null)
            manager.SetupContainers(minigameConfig.containerConfig.containers);

        LoadingScreenBehavior.CurrentLoadProgress = 0.5f;

        ItemObject.controller = this;

        trashSeparator = GarbageSeparator.CreateSeparator(minigameConfig, this);

        itemSpawner.SetupController(itemManager, objectSpawner, trashSeparator);

        LoadingScreenBehavior.CurrentLoadProgress = 0.8f;

        uiManager.SetupPowerupTimers(this);
    }

    private void UpdateGame()
    {
        switch (trashSeparator.GetSeparatorState())
        {
            case SeparatorState.Generating:
                UpdateTime();
                ConveyorSpeedUpdater();
                MoveAllItems();
                break;
        }
    }

    protected void ResetGame()
    {
        itemSpawner.ResetController();
        trashSeparator.ResetSetup();

        DisplayGame();

        UpdateStatistics();
        UpdateTime();
    }

    private void StartGame()
    {
        itemSpawner.ActivateSpawner();
        trashSeparator.StartGame();
        uiManager.SetupLives(trashSeparator.GetLivesLeft());
        SpawnNewItemObject();
    }

    private void DisplayGame()
    {
        Time.timeScale = timeScale;
        uiManager.DisplayGameScreen();
    }

    private void UpdateStatistics()
    {
        uiManager.UpdateScore(trashSeparator.GetSuccessesCount());
        uiManager.UpdateLives(trashSeparator.GetLivesLeft());
    }

    private void UpdateTime()
    {
        trashSeparator.UpdateClock(Time.deltaTime);
        uiManager.UpdateTimer(trashSeparator);
    }
    
    private void ConveyorSpeedUpdater()
    {
        conveyorAnimator.speed = conveyorSpeed * trashSeparator.GetSpeedMultiplier();
    }

    private void MoveAllItems()
    {
        ICollection<ItemObject> items = itemSpawner.GetAllActiveObjects();
        foreach (ItemObject i in items)
        {
            if(i is DragableTrashItem)
            {
                if(((DragableTrashItem) i).IsBeingDragged())
                {
                    continue;
                }
            }
            MoveGameObject(i.gameObject);
        }
    }

    private void MoveGameObject(GameObject obj)
    {
        float mult = Time.deltaTime * itemSpeed * trashSeparator.GetSpeedMultiplier();
        Vector3 translation = MOVEMENT_DIRECTION * mult;
        obj.transform.Translate(translation);
    }

    private void EndGame()
    {
        itemSpawner.EndGame();
        uiManager.UpdateEndScreen(trashSeparator.GetSuccessesCount(), trashSeparator.GetClockCount());
        uiManager.DisplayEndScreen();
    }

    private void PauseGame()
    {
        timeScale = Time.timeScale;
        Time.timeScale = 0f;
        uiManager.DisplayPauseScreen();
    }

    #endregion

}

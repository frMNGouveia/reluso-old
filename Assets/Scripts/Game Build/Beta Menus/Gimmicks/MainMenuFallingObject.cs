﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using UnityEngine;

public class MainMenuFallingObject : MonoBehaviour
{

    #region Constant Fields

    private const float SPEED_FALL_MULT = 30.0f;

    #endregion

    #region Variable Fields

    public float rotateSpeedMod, fallingSpeedMod;

    public float maximumY;

    private FallingObjectsGimmick gimmick;
    private RectTransform rectangleTransform;

    #endregion

    #region Unity Methods

    private void Start()
    {
        gimmick = GetComponentInParent<FallingObjectsGimmick>();
        rectangleTransform = GetComponent<RectTransform>();
    }

    void Update()
    {
        if(gameObject.activeSelf)
        {
            gameObject.transform.Rotate(Vector3.forward * gimmick.rotateSpeed * rotateSpeedMod, Space.Self);
            gameObject.transform.Translate(Vector3.down * Time.deltaTime * gimmick.fallingSpeed * fallingSpeedMod * SPEED_FALL_MULT, Space.World);
        }
        if (rectangleTransform.anchoredPosition.y < maximumY)
        {
            gimmick.DestroyFallingObject(gameObject);
        }
    }

    #endregion

    #region Public Methods
    #endregion
 
    #region Private Methods
    #endregion
	
}

﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FallingObjectsGimmick : MonoBehaviour
{
    #region Constant Fields

    #endregion

    #region Variable Fields

    [Range(0.0f, 5.0f)]
    public float fallingSpeed, rotateSpeed;

    public float fallingSpeedMinMult, fallingSpeedMaxMult, rotateSpeedMinMult, rotateSpeedMaxMult;
    public float spawnTimeMin, spawnTimeMax;
    public int maximumNumberOfObjects;

    private int objCounter = 0;
    private float canvasWidth;

    #endregion

    #region Unity Methods

    void Start()
    {
        canvasWidth = GetComponentInParent<RectTransform>().rect.width;
        for (int i = 0; i < maximumNumberOfObjects; i++)
            StartCoroutine(CreateFallingObject());
    }

    #endregion

    #region Public Methods

    public void DestroyFallingObject(GameObject obj)
    {
        Destroy(obj);
        StartCoroutine(CreateFallingObject());
    }

    #endregion
 
    #region Private Methods

    private MainMenuFallingObject InstanceFallingObject()
    {
        //Create object
        GameObject obj = new GameObject("Falling Object " + objCounter++);
        obj.SetActive(false);

        //Set Anchors, Parent and Scale
        RectTransform trans = obj.AddComponent<RectTransform>();
        trans.anchorMax = new Vector2(0f, 1f);
        trans.anchorMin = new Vector2(0f, 1f);


        trans.SetParent(gameObject.transform);
        trans.localScale = trans.localScale * 1.8f;

        //Add image
        Image img = obj.AddComponent<Image>();
        img.sprite = GetRandomTrashItemSprite();
        img.SetNativeSize();
        img.color = new Color(img.color.r, img.color.g, img.color.b, 0.6f);

        //Set Canvas as parent
        Canvas canvas = this.GetComponentInParent<Canvas>();
        obj.transform.SetParent(gameObject.transform);
        

        //Increase counter
        objCounter++;

        //Add Script and set Maximum Height
        MainMenuFallingObject fallObj = obj.AddComponent<MainMenuFallingObject>();
        fallObj.maximumY = -canvas.GetComponent<RectTransform>().rect.height;

        return fallObj;
    }

    private Vector3 GetRandomTopPosition(RectTransform trans)
    {
        float x = trans.rect.width / 2;
        float randomPadding = Random.Range(0f, canvasWidth - x);
        x += randomPadding;
        return new Vector3(x, trans.rect.height / 2, 0f);
    }

    private Sprite GetRandomTrashItemSprite()
    {
        int index = Random.Range(0, ItemUtil.ALL_TRASH_ITEMS.Length);
        return ItemUtil.ALL_TRASH_ITEMS[index].sprite;
    }

    private IEnumerator CreateFallingObject()
    {
        yield return new WaitForSeconds(RandomWaitPeriod());

        MainMenuFallingObject obj = InstanceFallingObject();

        //Set position
        RectTransform trans = obj.gameObject.GetComponent<RectTransform>();
        trans.anchoredPosition = GetRandomTopPosition(trans);

        //Set speed mods
        obj.fallingSpeedMod = Random.Range(fallingSpeedMinMult, fallingSpeedMaxMult);
        obj.rotateSpeedMod = Random.Range(rotateSpeedMinMult, rotateSpeedMaxMult);

        //Set Object active
        obj.gameObject.SetActive(true);
    }

    private float RandomWaitPeriod()
    {
        return Random.Range(spawnTimeMin, spawnTimeMax);
    }

    private bool ShouldSpawnObject()
    {
        return objCounter < maximumNumberOfObjects;
    }

    #endregion
	
}

﻿/**
* Created By: Francisco Miguel Nunes Gouveia
* Created On: 
*/
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;
using UnityEngine.UI;
using static LanguageButton;
using static LanguageButton.LanguageOption;

public class BetaOptionsMenu : Menu
{
    #region Constant Fields

    private static readonly LanguageOption[] debugModeActivationSeq = {En, En, Pt, En, Pt, Pt};

    #endregion

    #region Private Classes

    private class OptionsMenuDisplay : SingleMenuDisplay
    {

        private GameObject display;

        public OptionsMenuDisplay(GameObject display)
        {
            this.display = display;
        }

        public override void Display()
        {
            if (display != null)
                display.SetActive(true);
        }

        public override void Hide()
        {
            if (display != null)
                display.SetActive(false);
        }

    }

    #endregion

    #region Variable Fields

    public GameObject mainOptionsMenu, soundOptionsMenu, languageOptionsMenu;

    public GameObject languageButtons;

    public GameObject outline;

    private OptionsMenuDisplay mainOptionsDisplay, soundOptionsDisplay, languageOptionsDisplay;

    private BetaMenuController controller;

    private LanguageButton currentButton;
    private LanguageOption language = En;

    private int debugCounter = 0;

    #endregion

    #region Unity Methods

    void Start()
    {
        mainOptionsDisplay = new OptionsMenuDisplay(mainOptionsMenu);
        soundOptionsDisplay = new OptionsMenuDisplay(soundOptionsMenu);
        languageOptionsDisplay = new OptionsMenuDisplay(languageOptionsMenu);

        controller = BetaMenuController.GetMenuController(gameObject);
        
        soundOptionsMenu.SetActive(false);
        languageOptionsMenu.SetActive(false);
        outline.SetActive(false);

        currentButton = GetLanguageButton(PersistenceManager.languagePref);
        HighlightCurrentButton();

        //TEMPORARY DISABLES:
        Button[] buttons = gameObject.GetComponentsInChildren<Button>();
        Button soundButton = null;
        foreach (Button b in buttons)
            if (b.gameObject.name.StartsWith("Sound"))
                soundButton = b;
        if (soundButton != null)
            BetaMenuController.DisableButton(soundButton);
    }

    #endregion

    #region Constructor Methods
    #endregion

    #region Public Methods

    public override IMenuDisplay GetFirstMenuDisplay()
    {
        if(mainOptionsDisplay == null)
            mainOptionsDisplay = new OptionsMenuDisplay(mainOptionsMenu);
        return mainOptionsDisplay;
    }

    public void PressSoundOptionsButton()
    {
        //if (soundOptionsDisplay == null)
            //soundOptionsDisplay = new OptionsMenuDisplay(soundOptionsMenu);
        controller.Display(soundOptionsDisplay);
    }

    public void PressLanguageOptionsButton()
    {
        //if (languageOptionsDisplay == null)
            //languageOptionsDisplay = new OptionsMenuDisplay(languageOptionsMenu);
        controller.Display(languageOptionsDisplay);
    }

    [SerializeField]
    public void PressLanguageSelector(GameObject obj)
    {
        LanguageButton b = obj.GetComponent<LanguageButton>();

        //Highlight Button, un-Highlight Current Button
        DeHighlightCurrentButton();
        currentButton = b;
        HighlightCurrentButton();

        //Deal with Language Persistance
        PersistenceManager.SaveLanguagePref(b.language);

        //Update Locale
        PersistenceManager.UpdateLanguageLocale();

        //Update Debug Sequence
        UpdateDebugModeActivationSequence(b.language);
    }

    #endregion

    #region Protected Methods

    protected override void DisplayMenu()
    {
        gameObject.SetActive(true);
    }

    protected override void HideMenu()
    {
        gameObject.SetActive(false);
    }

    #endregion

    #region Private Methods

     private LanguageButton GetLanguageButton(LanguageOption lang)
    {
        LanguageButton[] buttons = languageButtons.GetComponentsInChildren<LanguageButton>();

        foreach (LanguageButton lb in buttons)
            if (lb.language == lang)
                return lb;

        return null;
    }

    private LanguageOption GetCurrentLanguage()
    {
        return language;
    }

    private void HighlightCurrentButton()
    {
        //Todo: Improve
        outline.SetActive(true);
        outline.transform.localPosition = currentButton.transform.localPosition;
    }

    private void DeHighlightCurrentButton()
    {
        //Todo: Improve
        outline.SetActive(false);
    }

    private void UpdateDebugModeActivationSequence(LanguageOption lang)
    {
        if (lang == debugModeActivationSeq[debugCounter])
            debugCounter++;
        else
            debugCounter = 0;
        if (debugCounter == debugModeActivationSeq.Length)
            BetaGameUtil.isInDebugMode = true;
        controller.SetDebugModeText();
    }

    #endregion

}

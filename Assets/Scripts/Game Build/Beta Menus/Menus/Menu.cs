﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using UnityEngine;

public abstract class Menu : MonoBehaviour, IMenu
{

    #region Variable Fields

    private bool isActive;

    #endregion

    #region Public Methods

    public void Activate()
    {
        isActive = true;
        DisplayMenu();
    }

    public void Disable()
    {
        isActive = false;
        HideMenu();
    }

    protected abstract void DisplayMenu();

    protected abstract void HideMenu();

    public abstract IMenuDisplay GetFirstMenuDisplay();

    public bool IsMenuActive()
    {
        return isActive;
    }

    #endregion
}

﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.UI;

public class BetaMainMenu : Menu
{

    #region Constant Fields

    private const string APP_VERSION_TYPE = "BETA";
    private const string APP_VERSION_DEFAULT = "D0.2.0";

    private const string SCENE_NAME_GARBAGE_SEPARATOR = "BetaGarbageSparatorScene";

    private const string DRAG_INSTRUCTION_1 = "Drag the recyclable items into the correct container.";
    private const string CLICK_INSTRUCTION_1 = "Click on the button matching a trash item's type before it reaches the container.";

    private const string MINIGAME_INSTRUCTION_1 = "If an item enters the wrong container, you lose a life.";
    private const string MINIGAME_INSTRUCTION_2 = "Once you lose all lives, the game ends." + "\n" + "Good game!";

    private const string DIALOG_EXIT_TITLE = "Exit Game?";
    private const string DIALOG_EXIT_DESCRIPTION = "Are you sure you want to exit the game?";
    private const string DIALOG_EXIT_POSITIVE_BUTTON = "Exit";
    private const string DIALOG_EXIT_NEGATIVE_BUTTON = "Cancel";

    private const string INSTRUCTION_COUNT_SEPARATOR = "/";

    private static readonly string[] CLICK_INSTRUCTIONS = { CLICK_INSTRUCTION_1, MINIGAME_INSTRUCTION_1, MINIGAME_INSTRUCTION_2 };
    private static readonly string[] DRAG_INSTRUCTIONS = { DRAG_INSTRUCTION_1, MINIGAME_INSTRUCTION_1, MINIGAME_INSTRUCTION_2 };

    #endregion

    #region Private Classes

    private class MainMenuDisplay : SingleMenuDisplay
    {

        private BetaMainMenu menu;

        public MainMenuDisplay(BetaMainMenu menu)
        {
            this.menu = menu;
        }
        
        public override void Display()
        {
            menu.versionText.gameObject.SetActive(true);
        }

        public override void Hide()
        {
            menu.versionText.gameObject.SetActive(false);
        }
    }

    #endregion

    #region Variable Fields

    public Text versionText;

    public Button exitButton;

    //Swap for Lists?
    public Button playGameButton;
    public Button infinityModeButton;
    public Button achievementsButton;
    public Button optionsButton;

    public Menu playGameMenu;
    public Menu infinityModeMenu;
    public Menu achievementsMenu;
    public Menu optionsMenu;

    public DialogBox exitDialog;

    //TODO: THIS
    private MainMenuDisplay mainMenuDisplay;

    private BetaMenuController controller;

    private Canvas canvas;

    private bool isDebug = false;

    [SerializeField]
    private LocalizedDialogData exitDialogData;

    protected string version;

    /*
    //DIFFICULTIES AND INSTRUCTIONS
    public GameObject difficultyUpButton, difficultyDownButton;

    private Difficulty[] difficulties;
    private int difficultyCounter;
    public TextMeshProUGUI instructionText, instructionCountText, difficultyText;

    public GameObject mainMenu, transitionMenu, configurationMenu, instructionMenu;
    */
    #endregion

    #region Unity Methods

    private void Awake()
    {
        canvas = GetComponentInParent<Canvas>();
        controller = BetaMenuController.GetMenuController(gameObject);
        mainMenuDisplay = new MainMenuDisplay(this);

        LoadingScreenBehavior.CurrentLoadProgress = 0.3f;

        version = Application.version;
        if (version == null)
            version = APP_VERSION_DEFAULT;
        version = APP_VERSION_TYPE + " " + version;
        if (versionText != null)
            versionText.text = version;

        if (!ShouldEnableInfinityButton())
            BetaMenuController.DisableButton(infinityModeButton);

        LoadingScreenBehavior.CurrentLoadProgress = 0.6f;

        //TEMPORARY DISABLES:
        BetaMenuController.DisableButton(playGameButton);
        BetaMenuController.DisableButton(achievementsButton);

        LoadingScreenBehavior.CurrentLoadProgress = 0.9f;
    }

    void Start()
    {
    }

    private void Update()
    {

        //ADD:
        if(isDebug != BetaGameUtil.isInDebugMode)
        {
            isDebug = BetaGameUtil.isInDebugMode;
            if (ShouldEnableInfinityButton())
                BetaMenuController.EnableButton(infinityModeButton);
            else
                BetaMenuController.DisableButton(infinityModeButton);
        }
        
    }

    #endregion

    #region Public Methods

    public override IMenuDisplay GetFirstMenuDisplay()
    {
        if(mainMenuDisplay == null)
            mainMenuDisplay = new MainMenuDisplay(this);
        return mainMenuDisplay;
    }

    public void PressExitGameButton()
    {
        exitDialog.onPositiveButtonClick = Application.Quit;
        exitDialog.onNegativeButtonClick = DialogBox.DO_NOTHING;
        exitDialog.DisplayDialog(exitDialogData);
    }

    public void PressPlayGameButton()
    {
        //TODO: THIS
    }

    public void PressInfinityButton()
    {
        //TODO: THIS
        BetaGameUtil.FadeAndLoadScene(BetaGameUtil.SCENE_INDEX_TRASH_SEPARATOR, canvas, this);
    }

    public void PressOptionsButton()
    {
        controller.DisplayMenu(optionsMenu);
    }

    public void PressAchievementsButton()
    {
        //TODO: THIS
    }

    #endregion

    #region Protected Methods

    protected override void DisplayMenu()
    {
        gameObject.SetActive(true);
    }

    protected override void HideMenu()
    {
        gameObject.SetActive(false);
    }

    #endregion

    #region Private Methods

    private bool ShouldEnableInfinityButton()
    {
        //ADD Progression check
        bool progression = true;

        return progression && BetaGameUtil.isInDebugMode;
    }
    #endregion


    /*
    public void ClickDemoButton()
    {
        currentDisplay = clickDemoFirstDisplay;
        UpdateDisplay();
    }

    public void DragDemoButton()
    {
        currentDisplay = dragDemoFirstDisplay;
        UpdateDisplay();
    }

    public void DifficultyUp()
    {
        if (difficultyCounter < difficulties.Length - 1)
            difficultyCounter++;
        UpdateDifficulty();
    }

    public void DifficultyDown()
    {
        if (difficultyCounter > 0)
            difficultyCounter--;
        UpdateDifficulty();
    }*/





    /*
    private void UpdateDisplay()
    {
        if(currentDisplay == null)
        {
            DisplayMainMenu();
            return;
        }

        mainMenu.SetActive(false);
        backButton.SetActive(true);
        
        forwardButton.SetActive(HasNextDisplay());

        DisplayType type = currentDisplay.GetDisplayType();
        switch(type)
        {
            case DisplayType.Standard:
                StandardDisplay standard = (StandardDisplay)currentDisplay;
                DisplayStandardMenu(standard);
                break;
            case DisplayType.Instruction:
                InstructionDisplay instruction = (InstructionDisplay)currentDisplay;
                DisplayInstructionMenu(instruction);
                break;
            case DisplayType.Transition:
                TransitionDisplay transition = (TransitionDisplay)currentDisplay;
                if(currentDisplay != null)
                    currentDisplayObject.SetActive(false);
                currentDisplayObject = transitionMenu;
                currentDisplayObject.SetActive(true);
                if (transition.GetSceneName() != null)
                    SceneManager.LoadScene(transition.GetSceneName());
                else
                {
                    Debug.Log("Error loading scene, no scene name given");
                    NavigateBack();
                }
                break;
            default:
                break;
        }
    }

    private bool HasNextDisplay()
    {
        if ((currentDisplay.GetDisplayType() == DisplayType.Instruction)
            && ((InstructionDisplay)currentDisplay).HasNext())
            return true;
        return currentDisplay.GetNextDisplay() != null;
    }

    private void DisplayMainMenu()
    {
        if (currentDisplayObject != null)
            currentDisplayObject.SetActive(false);
        currentDisplayObject = mainMenu;
        forwardButton.SetActive(false);
        backButton.SetActive(false);
        mainMenu.SetActive(true);

        SetupInfinityButton();
    }

    private void DisplayInstructionMenu(InstructionDisplay display)
    {
        DisplayMenu(instructionMenu);
        instructionText.text = display.CurrentInstruction();
        instructionCountText.text = "" + (display.CurrentIndex() + 1) + INSTRUCTION_COUNT_SEPARATOR + display.GetInstructionsCount();
    }

    private void DisplayStandardMenu(StandardDisplay display)
    {
        DisplayMenu(display.GetDisplayedUI());
    }

    private void DisplayMenu(GameObject menu)
    {
        if (currentDisplayObject != menu)
        {
            currentDisplayObject.SetActive(false);
            currentDisplayObject = menu;
            currentDisplayObject.SetActive(true);
        }
    }

    private void SetupInfinityButton()
    {
        Debug.Log(BetaStaticData.isInDebugMode);
        if (BetaStaticData.isInDebugMode)
            EnableButton(infinityModeButton);
        else
            DisableButton(infinityModeButton);

        //TODO FINISH THIS
    }

    /*
    private IDisplay GetClickDemoDisplay()
    {
        StandardDisplay d = new StandardDisplay(configurationMenu);
        InstructionDisplay i = new InstructionDisplay(CLICK_INSTRUCTIONS);
        d.SetNextDisplay(i);
        i.SetPreviousDisplay(d);
        TransitionDisplay t = new TransitionDisplay(SCE);
        i.SetNextDisplay(t);
        t.SetPreviousDisplay(i);
        return d;
    }*/

    /*private IDisplay GetDragDemoDisplay()
    {
        StandardDisplay d = new StandardDisplay(configurationMenu);
        InstructionDisplay i = new InstructionDisplay(DRAG_INSTRUCTIONS);
        d.SetNextDisplay(i);
        TransitionDisplay t = new TransitionDisplay(SCENE_NAME_CALLIBRATE_DRAG);
        i.SetNextDisplay(t);
        return d;
    }*/



    // DIFFICULTY HERE

    /*
    private void SetupDifficulty()
    {
        difficulties = GetDifficulties();
        Array.Sort(difficulties);
        difficultyCounter = 1;
        if (difficultyCounter >= difficulties.Length)
            difficultyCounter = 0;
        UpdateDifficulty();
    }

    private void UpdateDifficulty()
    {
        if (difficultyCounter >= 0 && difficultyCounter < difficulties.Length)
        {
            StaticData.currentGameDifficulty = difficulties[difficultyCounter];
            difficultyText.text = difficulties[difficultyCounter].ToString().Split(' ')[0]; ;
        }
    }

    private static Difficulty[] GetDifficulties()
    {
        return Resources.LoadAll<Difficulty>(RESOURCE_DIFFICULTY_PATH);
    }*/



    //Deprecated, Dead Code

    /*private IEnumerator UpdateLoading()
    {
        yield return new WaitForSeconds(0.3f);
        loadingCounter++;
        loadingCounter %= LOADING_DOTS.Length;
        loadingText.text = LOADING + LOADING_DOTS[loadingCounter];
    }*/

}

﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/

public interface IMenu
{

    IMenuDisplay GetFirstMenuDisplay();

    bool IsMenuActive();

    void Activate();

    void Disable();

}

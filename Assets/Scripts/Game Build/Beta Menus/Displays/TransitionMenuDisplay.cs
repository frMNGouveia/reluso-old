﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using UnityEngine;

public abstract class TransitionMenuDisplay : MenuDisplay
{

    #region Variable Fields

    private string sceneName;

    #endregion

    #region Constructor Methods

    public TransitionMenuDisplay(string sceneName) : base()
    {
        this.sceneName = sceneName;
    }

    #endregion

    #region Public Methods

    public override MenuDisplayType GetDisplayType()
    {
        return MenuDisplayType.Transition;
    }

    public string GetSceneName()
    {
        return sceneName;
    }

    #endregion


}

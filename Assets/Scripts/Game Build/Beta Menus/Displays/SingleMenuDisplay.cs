﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using UnityEngine;

public abstract class SingleMenuDisplay : MenuDisplay
{

    public override MenuDisplayType GetDisplayType()
    {
        return MenuDisplayType.Single;
    }

}

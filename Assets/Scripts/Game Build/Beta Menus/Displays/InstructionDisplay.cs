﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using UnityEngine;

public class InstructionDisplay
{
    #region Constant Fields
    #endregion

    #region Variable Fields
    #endregion

    #region Constructor Methods
    #endregion

    #region Public Methods
    #endregion

    #region Private Methods
    #endregion

    /*
    public bool HasNext()
    {
        if (instructions != null)
            return index < instructions.Length - 1;
        return false;
    }

    public bool HasPrevious()
    {
        if (instructions != null)
            return index > 0;
        return false;
    }

    public void IncrementIndex(int increment)
    {
        index += increment;
        if (instructions != null)
            if (index < 0)
                index = 0;
            else if (index >= instructions.Length)
                index = instructions.Length - 1;
    }

    public string CurrentInstruction()
    {
        if (IsIndexWithinBounds())
            return instructions[index];
        return "";
    }

    public int CurrentIndex()
    {
        return index;
    }

    public int GetInstructionsCount()
    {
        if (instructions != null)
            return instructions.Length;
        return -1;
    }

    private bool IsIndexWithinBounds()
    {
        if (instructions != null)
            return index >= 0 && index < instructions.Length;
        return false;
    }
    */

}

﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using UnityEngine;

using static MenuDisplay;

public interface IMenuDisplay
{

    MenuDisplayType GetDisplayType();

    void Display();

    void Hide();

}

﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using UnityEngine;

public abstract class MultipleMenuDisplay : MenuDisplay
{

    #region Variable Fields

    /*private string[] instructions;
    private int index;

    public InstructionMenuDisplay(string[] instructions)
    {
        this.instructions = instructions;
        index = 0;
    }*/

    #endregion

    #region Constructor Methods

    #endregion

    #region Public Methods

    public override MenuDisplayType GetDisplayType()
    {
        return MenuDisplayType.Multiple;
    }

    public abstract bool HasNext();

    public abstract bool HasPrevious();

    public abstract void DisplayNext();

    public abstract void DisplayPrevious();

    #endregion

    #region Private Methods

    #endregion

}

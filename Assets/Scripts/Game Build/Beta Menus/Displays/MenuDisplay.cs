﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using UnityEngine;

public abstract class MenuDisplay : IMenuDisplay
{

    #region Constant Fields

    public enum MenuDisplayType
    {
        Single,
        Multiple,
        Transition
    }

    #endregion

    #region Public Methods

    public abstract MenuDisplayType GetDisplayType();

    public abstract void Display();

    public abstract void Hide();

    #endregion

}

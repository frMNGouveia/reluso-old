﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 05/02/2021
*/
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Localization.Settings;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using static AssetUtil;

public class BetaMenuController : MonoBehaviour
{

    #region Private Classes

    private class MenuNav
    {

        public Menu menu;
        public LinkedList<IMenuDisplay> displays;

        public MenuNav(Menu menu, IMenuDisplay firstDisplay)
        {
            this.menu = menu;
            this.displays = new LinkedList<IMenuDisplay>();
            displays.AddFirst(firstDisplay);
        }

    }

    #endregion

    #region Variable Fields

    private LinkedList<MenuNav> menus;

    public Menu mainMenu;
    public GameObject navigationButtons;
    public GameObject forwardButton, backButton;

    public GameObject debugModeText;

    public GameObject[] allDisplays;

    #endregion

    #region Unity Methods

    public void Start()
    {
        BetaGameUtil.currentGameType = BetaGameUtil.GameType.Menu;
        this.menus = new LinkedList<MenuNav>();

        foreach (GameObject o in allDisplays)
            o.SetActive(false);

        Button buttons = navigationButtons.GetComponentInChildren<Button>();
        debugModeText.SetActive(BetaGameUtil.isInDebugMode);
        SetDebugModeText();

        SetupLanguageFromPersistance();
        
        DisplayMenu(mainMenu);
    }

    #endregion

    #region Public Methods

    //@pre: HasForward()
    public void NavigateForward()
    {
        IMenuDisplay currentDisplay = GetCurrentMenuDisplay();
        switch (currentDisplay.GetDisplayType())
        {
            case MenuDisplay.MenuDisplayType.Multiple:
                ((MultipleMenuDisplay)currentDisplay).DisplayNext();
                break;
            default:
                break;
        }

        SetupNavigationButtons();
    }

    //@pre: HasBack()
    public void NavigateBack()
    {

        IMenuDisplay currentDisplay = GetCurrentMenuDisplay();
        switch (currentDisplay.GetDisplayType())
        {
            case MenuDisplay.MenuDisplayType.Multiple:
                ((MultipleMenuDisplay)currentDisplay).DisplayPrevious();
                break;
            case MenuDisplay.MenuDisplayType.Single:
                currentDisplay.Hide();
                if (menus.Last.Value.displays.Count > 1)
                {
                    menus.Last.Value.displays.RemoveLast();
                }
                else if(menus.Count > 1)
                {
                    menus.Last.Value.menu.Disable();
                    menus.RemoveLast();
                    menus.Last.Value.menu.Activate();
                }
                currentDisplay = GetCurrentMenuDisplay();
                currentDisplay.Display();
                break;
            default:
                break;
        }

        SetupNavigationButtons();
    }

    public void DisplayMenu(Menu menu)
    {
        if (menu == null)
            return;
        IMenuDisplay display = menu.GetFirstMenuDisplay();

        MenuNav menuNav = new MenuNav(menu, display);

        if(menus.Count > 0)
            menus.Last.Value.menu.Disable();

        menus.AddLast(new LinkedListNode<MenuNav>(menuNav));
        menu.Activate();
        
        display.Display();
        SetupNavigationButtons();
    }

    public void Display(IMenuDisplay display)
    {
        MenuNav currentNav = menus.Last.Value;
        IMenuDisplay currentDisplay = currentNav.displays.Last.Value;
        if (currentDisplay != null)
            currentDisplay.Hide();

        currentNav.displays.AddLast(display);

        display.Display();
        SetupNavigationButtons();
    }

    public static void DisableButton(Button button)
    {
        button.interactable = false;
        TextMeshProUGUI text = button.gameObject.GetComponentInChildren<TextMeshProUGUI>();
        if (text != null)
            text.outlineColor = Color.black * button.colors.disabledColor;
    }

    public static void EnableButton(Button button)
    {
        button.interactable = true;
        TextMeshProUGUI text = button.gameObject.GetComponentInChildren<TextMeshProUGUI>();
        if (text != null)
            text.outlineColor = Color.black;
    }

    public static BetaMenuController GetMenuController(GameObject obj)
    {
        return obj.GetComponentInParent<BetaMenuController>();
    }

    public void SetDebugModeText()
    {
        if (BetaGameUtil.isInDebugMode)
            debugModeText.SetActive(true);
    }

    #endregion

    #region Private Methods

    private void SetupNavigationButtons()
    {
        forwardButton.gameObject.SetActive(HasForward());
        backButton.gameObject.SetActive(HasBack());
        navigationButtons.SetActive(true);
    }

    private bool HasForward()
    {
        MenuNav currentNav = menus.Last.Value;

        IMenuDisplay currentDisplay = null;
        if (currentNav.displays.Last != null)
            currentDisplay = currentNav.displays.Last.Value;
        if (currentDisplay == null)
            return false;

        if (currentDisplay.GetDisplayType() != MenuDisplay.MenuDisplayType.Multiple)
            return false;

        MultipleMenuDisplay mult = (MultipleMenuDisplay) currentDisplay;
        return mult.HasNext();
    }

    private bool HasBack()
    {
        MenuNav currentNav = menus.Last.Value;
        
        IMenuDisplay currentDisplay = null;
        if (currentNav.displays.Last != null)
            currentDisplay = currentNav.displays.Last.Value;
        if (currentDisplay == null)
            return false;

        if (currentDisplay.GetDisplayType() == MenuDisplay.MenuDisplayType.Multiple
            && ((MultipleMenuDisplay)currentDisplay).HasPrevious())
            return true;

        if (currentNav.displays.Count > 1)
            return true;

        return menus.Count > 1;
    }

    private IMenuDisplay GetCurrentMenuDisplay()
    {
        MenuNav currentNav = menus.Last.Value;

        IMenuDisplay currentDisplay = null;
        if (currentNav.displays.Last != null)
            currentDisplay = currentNav.displays.Last.Value;
        return currentDisplay;
    }

    private void SetupLocalizationLanguage()
    {
        LanguageButton.LanguageOption op = PersistenceManager.languagePref;
    }

    private void SetupLanguageFromPersistance()
    {
        if (LanguagePersistence.HasLocales())
            PersistenceManager.UpdateLanguageLocale();
        else
            StartCoroutine(WaitForLoadedLocales());

    }

    private IEnumerator WaitForLoadedLocales()
    {
        yield return new WaitForSeconds(0.1f);
        SetupLanguageFromPersistance();
    }

    #endregion

}

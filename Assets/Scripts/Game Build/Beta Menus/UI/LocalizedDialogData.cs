using System;
using UnityEngine;
using UnityEngine.Localization;

[Serializable]
public class LocalizedDialogData
{

    [SerializeField]
    public LocalizedString dialogTitle, dialogDescription, dialogPositive, dialogNegative;

}

﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class DialogBox : MonoBehaviour
{

    #region Variable Fields

    public static readonly Action DO_NOTHING = () => { };

    #endregion

    #region Variable Fields

    public TextMeshProUGUI titleText;

    public Text descriptionText;
    public Text positiveButtonText;
    public Text negativeButtonText;

    public Action onPositiveButtonClick = DO_NOTHING;
    public Action onNegativeButtonClick = DO_NOTHING;

    public bool disableOnPositiveClick = true;
    public bool disableOnNegativeClick = true;

    #endregion

    #region Unity Methods

    void Start()
    {

    }

    #endregion

    #region Public Methods

    public void OnPositiveButtonPressed()
    {
        onPositiveButtonClick();
        if(disableOnPositiveClick)
            this.gameObject.SetActive(false);
    }

    public virtual void OnNegativeButtonPressed()
    {
        onNegativeButtonClick();
        if (disableOnNegativeClick)
            this.gameObject.SetActive(false);
    }

    public void DisplayDialog(string title, string description, string positiveButtonText, string negativeButtonText)
    {
        SetTitle(title);
        SetDescription(description);
        SetPositiveButtonText(positiveButtonText);
        SetNegativeButtonText(negativeButtonText);

        this.gameObject.SetActive(true);
    }

    public void DisplayDialog(LocalizedDialogData data)
    {
        DisplayDialog(data.dialogTitle.GetLocalizedString(),
            data.dialogDescription.GetLocalizedString(),
            data.dialogPositive.GetLocalizedString(),
            data.dialogNegative.GetLocalizedString());
    }

    public void SetTitle(string title)
    {
        this.titleText.text = title;
    }

    public void SetDescription(string description)
    {
        this.descriptionText.text = description;
    }

    public virtual void SetPositiveButtonText(string positiveButtonText)
    {
        this.positiveButtonText.text = positiveButtonText.ToUpper();
    }

    public virtual void SetNegativeButtonText(string negativeButtonText)
    {
        this.negativeButtonText.text = negativeButtonText.ToUpper();
    }
    
    public static DialogBox InstantiateDialogBox(Action onPosButtoneClick, Action onNegButtoneClick, Canvas canvas)
    {
        DialogBox dialog = Instantiate(Resources.Load<DialogBox>(AssetUtil.UI_DIALOG_PREFAB), canvas.transform);
        dialog.onPositiveButtonClick = onPosButtoneClick;
        dialog.onNegativeButtonClick = onNegButtoneClick;
        return dialog;
    }

    #endregion

}

﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using System;
using UnityEngine;

public class LanguageButton : MonoBehaviour
{
    #region Constant Fields


    [Serializable]
    public enum LanguageOption
    {
        En,
        Pt
    }

    #endregion

    #region Variable Fields

    [SerializeField]
    public LanguageOption language;

    #endregion

}

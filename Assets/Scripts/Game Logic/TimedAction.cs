using System;
using System.Collections.Generic;
using UnityEngine;

//Code inspired by Trigger an Action after some Time, by Code Monkey https://www.youtube.com/watch?v=1hsppNzx7_0
public class TimedAction
{

    #region Internal Classes

    public class TimedActionObject : MonoBehaviour
    {

        public Action update;

        private void Update()
        {
            update?.Invoke();
        }

    }

    #endregion

    #region Constant Fields

    private const string TIMED_ACTIONS_OBJECT_NAME = "Timed Functions";
    private const string TIMED_ACTION_OBJECT_NAME = "Timed Function";

    #endregion

    #region Variable Fields

    private static List<TimedAction> activeActions;
    private static GameObject timedActionCol;

    private Action action;
    private float delay, timeLeft;
    private GameObject gameObject;
    private bool hasActed, isPaused;

    #endregion

    #region Constructor Methods

    private TimedAction(Action action, float delay)
    {
        this.action = action;
        this.delay = delay;
        ResetAction();
    }

    #endregion

    #region Static Methods

    public static TimedAction CreateAction(Action action, float delay)
    {
        TimedAction timedAction = new TimedAction(action, delay);
        return timedAction;
    }

    public static TimedAction StartAction(TimedAction timedAction)
    {
        if (timedAction == null)
            return null;
        Init();
        GameObject gameObject = new GameObject(TIMED_ACTION_OBJECT_NAME, typeof(TimedActionObject));
        gameObject.transform.parent = timedActionCol.transform;
        gameObject.GetComponent<TimedActionObject>().update = timedAction.Update;
        timedAction.gameObject = gameObject;
        activeActions.Add(timedAction);

        return timedAction;
    }

    public static TimedAction RestartAction(TimedAction timedAction)
    {
        if (timedAction == null)
            return null;
        timedAction.ResetAction();
        return StartAction(timedAction);
    }

    public static TimedAction CreateAndStartAction(Action action, float delay)
    {
        TimedAction timedAction = CreateAction(action, delay);
        return StartAction(timedAction);
    }

    public static void StopAction(TimedAction action)
    {
        if(action != null)
            action.DestroySelf();
    }

    public static void PauseAction(TimedAction action)
    {
        if (action != null)
            action.isPaused = true;
    }

    public static void UnpauseAction(TimedAction action)
    {
        if (action != null)
            action.isPaused = false;
    }

    private static void RemoveAction(TimedAction action)
    {
        Init();
        activeActions.Remove(action);
    }

    private static void Init()
    {
        if (timedActionCol != null)
            return;
        timedActionCol = new GameObject(TIMED_ACTIONS_OBJECT_NAME);
        activeActions = new List<TimedAction>();
    }

    #endregion

    #region Public Methods

    public void Update()
    {
        if (hasActed || isPaused)
            return;
        timeLeft -= Time.deltaTime;
        if (timeLeft > 0)
            return;
        action();
        DestroySelf();
    }

    public void Delay(float delay)
    {
        timeLeft += delay;
    }

    public float GetTimeLeft()
    {
        return timeLeft;
    }

    public float GetDelay()
    {
        return delay;
    }

    public Action GetAction()
    {
        return action;
    }

    #endregion

    #region Private Methods

    private void DestroySelf()
    {
        hasActed = true;
        if(gameObject != null)
            UnityEngine.Object.Destroy(gameObject);
        RemoveAction(this);
    }

    private void ResetAction()
    {
        timeLeft = delay;
        this.gameObject = null;
        this.hasActed = false;
        this.isPaused = false;
    }

    #endregion

}

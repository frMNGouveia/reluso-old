﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using System.Collections.Generic;
using UnityEngine;
using static AssetUtil;
using static ItemUtil;

public class SimpleTrashGenerator: IItemGenerator<TrashItem>
{

    //POSSIBLY OPTIMIZE THIS CLASS BY USING AN INT ARRAY INSTEAD? AN ARRAY OF INDEXES?

    #region Constant Fields
    #endregion

    #region Variable Fields

    private HashSet<TrashType> itemTypes;
    private TrashItem[] trashItems;

    #endregion

    #region Constructor Methods

    public SimpleTrashGenerator(HashSet<TrashType> itemTypes)
    {
        this.itemTypes = itemTypes;
        FilterTrashItemsByType();
    }

    #endregion

    #region Public Methods
    
    public int GetNumberOfItems()
    {
        return trashItems.Length;
    }

    public TrashItem GetRandomTrashItem()
    {
        return trashItems[GetRandomTrashItemIndex()];
    }

    #endregion

    #region Private Methods

    private int GetRandomTrashItemIndex()
    {
        return Random.Range(0, trashItems.Length);
    }

    private void FilterTrashItemsByType()
    {
        List<TrashItem> trashItemsList = new List<TrashItem>();
        foreach (TrashItem t in ALL_TRASH_ITEMS)
            if (itemTypes.Contains(t.trashType))
                trashItemsList.Add(t);
        trashItems = trashItemsList.ToArray();
    }

    #endregion

}

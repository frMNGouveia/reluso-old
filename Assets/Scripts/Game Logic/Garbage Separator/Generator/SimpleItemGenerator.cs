using System.Collections.Generic;
using UnityEngine;

public class SimpleItemGenerator : IItemGenerator<Item>
{

    #region Variable Fields

    private GeneratorConfig config;
    private List<Item> powerupItems, trashItems;
    private int maxWeight;

    #endregion

    #region Constructor Methods

    public SimpleItemGenerator(GeneratorConfig config, Item[] items)
    {
        this.config = config;
        trashItems = new List<Item>();
        powerupItems = new List<Item>();
        foreach(Item i in items)
        {
            if (i is TrashItem)
                trashItems.Add(i);
            else if (i is PowerupItem)
                powerupItems.Add(i);
        }

        maxWeight = config.powerupWeight + config.trashItemWeight;
    }

    #endregion

    #region Public Methods

    public int GetNumberOfItems()
    {
        return trashItems.Count;
    }

    public Item GetRandomTrashItem()
    {
        List<Item> list = GetRandomItemList();
        return list[GetRandomIndex(list)];
    }

    #endregion

    #region Private Methods

    private List<Item> GetRandomItemList()
    {
        int i = Random.Range(0, maxWeight);
        if (i < config.powerupWeight && powerupItems.Count > 0)
            return powerupItems;
        else
            return trashItems;
    }

    private int GetRandomIndex(List<Item> list)
    {
        return Random.Range(0, list.Count);
    }


    #endregion

}



public interface IItemGenerator<T> where T: Item
{

    int GetNumberOfItems();

    T GetRandomTrashItem();

}

﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Class for the Garbage Separator game rules and behaviour
*/
using System.Collections.Generic;
using UnityEngine;

public abstract class GarbageSeparator : IGarbageSeparator
{
    
    #region Enums

    public enum SeparatorGameType
    {
        Leveled,
        Infinity
    }

    public enum EndState
    {
        Win,
        Loss
    }

    public enum SeparatorState
    {
        Initial,
        Setup,
        Generating,
        Paused,
        Ended
    }

    public enum PowerupEffect
    {
        ExtraLife,
        SlowTime,
        Magnetism
    }

    #endregion

    #region Constant Fields

    public const int CONTAINER_INDEX_START = 1;

    #endregion

    #region Variable Fields

    public SeparatorConfig Config { get; }

    protected SeparatorState currentState;
    protected EndState endState;

    protected int lives, fails, successes;
    protected float speedMult;

    protected List<SeparatedTrashItem> trashItemsHistory;
    protected HashSet<TrashType> trashTypes;
    protected IItemGenerator<Item> trashGenerator;

    protected MultiTimedAction speedMultActions;
    protected Dictionary<int, PowerupStatus> powerupManager;

    protected ISeparatorStateManager stateManager;

    #endregion

    #region Constructor Methods

    protected GarbageSeparator(SeparatorConfig config): this(config, null)
    {

    }

    protected GarbageSeparator(SeparatorConfig config, ISeparatorStateManager stateManager)
    {
        ChangeState(SeparatorState.Initial);
        this.Config = config;
        this.stateManager = stateManager;
        SetupPowerupManager();
        SetupItemGenerator();
    }

    #endregion

    #region Static Methods

    public static IGarbageSeparator CreateSeparator(SeparatorConfig config)
    {
        if (config is GSLevelledModeConfig)
            return new LevelledSeparator((GSLevelledModeConfig) config);
        else
            return new InfinitySeparator(config);
    }

    public static IGarbageSeparator CreateSeparator(SeparatorConfig config, ISeparatorStateManager stateManager)
    {
        if (config is GSLevelledModeConfig)
            return new LevelledSeparator((GSLevelledModeConfig)config, stateManager);
        else
            return new InfinitySeparator(config, stateManager);
    }

    #endregion

    #region Public Methods

    public virtual void ResetSetup()
    {
        SetupStatistics();
        SetupDifficulty();
        ChangeState(SeparatorState.Setup);
    }

    public virtual int GetSuccessesCount()
    {
        return successes;
    }

    public virtual int GetFailsCount()
    {
        return fails;
    }

    public virtual int GetLivesLeft()
    {
        return lives;
    }

    public virtual bool PlaceTrashInContainer(TrashItem trashItem, Container container)
    {
        SeparatedTrashItem item = new SeparatedTrashItem(trashItem, container);

        if (container == null)
            OutOfBounds();
        else if (item.CorrectlySeparated)
            SortingSuccess();
        else
            SortingFailure();
        trashItemsHistory.Add(item);


        return item.CorrectlySeparated;
    }

    public virtual List<SeparatedTrashItem> SeparatedTrashItemsHistory()
    {
        return trashItemsHistory;
    }

    public virtual bool TrashOutOfBounds(TrashItem trashItem)
    {
        return PlaceTrashInContainer(trashItem, null);
    }

    public virtual SeparatorState GetSeparatorState()
    {
        return currentState;
    }

    public virtual float GetSpeedMultiplier()
    {
        return speedMult;
    }

    public virtual void MultiplySpeed(float multiplier)
    {
        speedMult *= multiplier;
    }

    public virtual Item GenerateItem()
    {
        return trashGenerator.GetRandomTrashItem();
    }

    public virtual EndState GetEndState()
    {
        return endState;
    }

    public virtual void StartGame()
    {
        ChangeState(SeparatorState.Generating);
        if (speedMultActions != null)
            speedMultActions.StartAll();
    }

    public virtual void Pause()
    {
        ChangeState(SeparatorState.Paused);
    }

    public virtual void Unpause()
    {
        ChangeState(SeparatorState.Generating);
    }

    public virtual void ActivatePowerup(PowerupItem powerup)
    {
        if(powerup.effect is TempPowerupEffect)
        {
            PowerupStatus status = GetStatusFromPowerupManager(powerup);
            if (status != null)
            {
                if(!status.active)
                {
                    powerup.effect.ActivatePowerup(this);
                    status.active = true;
                }
                else
                    TimedAction.StopAction(status.deactivateAction);
                TimedAction.RestartAction(status.deactivateAction);
            }
        }
        else
            powerup.effect.ActivatePowerup(this);
    }

    public virtual void DeactivatePowerup(PowerupItem powerup)
    {
        if (powerup.effect is TempPowerupEffect)
        {
            ((TempPowerupEffect)powerup.effect).DeactivatePowerup(this);

            PowerupStatus status = GetStatusFromPowerupManager(powerup);
            if (status != null)
                status.active = false;
        }
    }

    public virtual ICollection<PowerupStatus> GetTemporaryPowerupStatuses()
    {
        return powerupManager.Values;
    }

    public virtual void SetLives(int lives)
    {
        this.lives = lives;
    }

    public virtual void EndGame(EndState endState)
    {
        ChangeState(SeparatorState.Ended);
        this.endState = endState;

        speedMultActions.StopAll();

        foreach (PowerupStatus s in powerupManager.Values)
        {
            TimedAction.StopAction(s.deactivateAction);
            s.active = false;
        }
    }

    public abstract void UpdateClock(float deltaTime);

    public abstract float GetClockCount();

    #endregion

    #region Protected Methods

    protected virtual void SortingSuccess()
    {
        successes++;
    }

    protected virtual void SortingFailure()
    {
        RegularFailure();
    }

    protected virtual void OutOfBounds()
    {
        RegularFailure();
    }

    protected virtual void RegularFailure()
    {
        fails++;
        lives--;
        if (lives <= 0)
            EndGame(EndState.Loss);
            
    }

    protected virtual void SetupStatistics()
    {
        trashItemsHistory = new List<SeparatedTrashItem>();
        fails = 0;
        successes = 0;
    }

    protected virtual void SetupDifficulty()
    {
        lives = Config.difficulty.numberOfLives;
        speedMult = Config.difficulty.startingSpeedMultiplier;

        List<TimedAction> speedChanges = Config.difficulty.GenerateSpeedChanges(this);
        if (Config.difficulty.repeatChanges)
            speedMultActions = new RepeatMultiTimedAction(speedChanges);
        else
            speedMultActions = new MultiTimedAction(speedChanges);
    }

    protected virtual void SetupItemGenerator()
    {
        Item[] items = ItemUtil.GetItemsForConfiguration(Config);
        trashGenerator = new SimpleItemGenerator(Config.generatorConfig, items);

        PopulatePowerupManager(items);
    }

    protected virtual void ChangeState(SeparatorState newState)
    {
        this.currentState = newState;
        if (stateManager != null)
            stateManager.OnSeparatorStateChange(newState);
    }

    protected virtual void SetupPowerupManager()
    {
        powerupManager = new Dictionary<int, PowerupStatus>();
    }

    protected virtual void PopulatePowerupManager(Item[] items)
    {
        foreach (Item i in items)
            if(i is PowerupItem && ((PowerupItem) i).effect is TempPowerupEffect)
                AddPowerupToManager((PowerupItem) i);
    }

    protected virtual void AddPowerupToManager(PowerupItem powerup)
    {
        TempPowerupEffect tempEffect = (TempPowerupEffect)powerup.effect;
        PowerupStatus status = new PowerupStatus();
        status.powerup = powerup;
        status.active = false;
        status.deactivateAction = TimedAction.CreateAction(() => this.DeactivatePowerup(powerup), tempEffect.durationInSeconds);
        powerupManager.Add(powerup.internalID, status);
    }

    protected virtual PowerupStatus GetStatusFromPowerupManager(PowerupItem powerup)
    {
        PowerupStatus status = null;
        powerupManager.TryGetValue(powerup.internalID, out status);
        return status;
    }

    #endregion

}

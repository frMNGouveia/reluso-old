﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Interface for the Garbage Separator game rules and behaviour
*/
using System.Collections.Generic;
using static GarbageSeparator;

public interface IGarbageSeparator
{

    void ResetSetup();

    void StartGame();

    SeparatorState GetSeparatorState();

    EndState GetEndState();

    List<SeparatedTrashItem> SeparatedTrashItemsHistory();

    bool PlaceTrashInContainer(TrashItem trashItem, Container container);

    bool TrashOutOfBounds(TrashItem trashItem);

    Item GenerateItem();

    int GetSuccessesCount();

    int GetFailsCount();

    int GetLivesLeft();

    float GetSpeedMultiplier();

    void MultiplySpeed(float multiplier);

    float GetClockCount();

    void UpdateClock(float deltaTime);

    void Pause();

    void Unpause();

    void ActivatePowerup(PowerupItem powerup);

    void DeactivatePowerup(PowerupItem powerup);

    ICollection<PowerupStatus> GetTemporaryPowerupStatuses();

    void SetLives(int lives);

    void EndGame(EndState endState);

}

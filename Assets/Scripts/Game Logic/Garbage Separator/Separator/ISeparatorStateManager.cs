using static GarbageSeparator;

public interface ISeparatorStateManager
{

    void OnSeparatorStateChange(SeparatorState newState);

}

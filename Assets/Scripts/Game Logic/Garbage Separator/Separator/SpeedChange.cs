﻿/**
* Created By: Francisco Miguel Nunes Gouveia
* A class that represents a change (generally an increase) in the game's speed after a certain ammount of time.
* It is designed to be used in the DynamicDifficulty serialized object.
*/
using System;


/// <summary>
/// The <c>SpeedChange</c> class that represents a change (generally an increase) in the game's speed after a certain ammount of time.
/// It is meant to be used in the <c>Difficulty</c> and <c>DynamicDifficulty</c> serialized objects.
/// </summary>
[Serializable]
public class SpeedChange : IComparable {

    //Timestamp after which the speed is changed
    public float delay;
    //The respective value used to multiply the speed
    public float speedMult;

    //Compares speedincreases by timestamp,
    public int CompareTo(object obj)
    {
        if(typeof(SpeedChange) != obj.GetType())
            throw new NotImplementedException();
        SpeedChange inc = (SpeedChange) obj;

        float result = delay - inc.delay;
        if (result < 0)
            return -1;
        if (result > 0)
            return 1;
        return 0;
    }
}

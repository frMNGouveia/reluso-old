public class InfinitySeparator : GarbageSeparator
{
    #region Variable Fields

    private float timer;

    #endregion

    #region Constructor Methods

    public InfinitySeparator(SeparatorConfig config) : this(config, null)
    {

    }

    public InfinitySeparator(SeparatorConfig config, ISeparatorStateManager stateManager) : base(config, stateManager)
    {

    }

    #endregion

    #region Public Methods

    public override void UpdateClock(float deltaTime)
    {
        timer += deltaTime;
    }

    public override float GetClockCount()
    {
        return timer;
    }

    #endregion

    #region Protected Methods

    protected override void SetupStatistics()
    {
        base.SetupStatistics();
        timer = 0f;
    }

    #endregion

}

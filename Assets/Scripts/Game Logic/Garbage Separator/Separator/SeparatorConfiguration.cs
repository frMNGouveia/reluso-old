﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using System;

[Serializable]
public class SeparatorConfiguration
{

    #region Variable Fields

    public Difficulty difficulty;
    public ContainerConfig containerConfigurations;

    #endregion

}

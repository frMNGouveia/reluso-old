using static GarbageSeparator.SeparatorState;
using static GarbageSeparator.EndState;

public class LevelledSeparator : GarbageSeparator
{
    
    #region Variable Fields

    private TimedAction winGameAction;

    #endregion

    #region Constructor Methods

    public LevelledSeparator(GSLevelledModeConfig config) : this(config, null)
    {

    }

    public LevelledSeparator(GSLevelledModeConfig config, ISeparatorStateManager stateManager) : base(config, stateManager)
    {
        winGameAction = TimedAction.CreateAction(() => EndGame(Win), config.timeOut);
    }

    #endregion

    #region Public Methods

    public override void UpdateClock(float deltaTime)
    {

    }

    public override float GetClockCount()
    {
        return winGameAction.GetTimeLeft();
    }

    public override void EndGame(EndState endState)
    {
        base.EndGame(endState);
        TimedAction.StopAction(winGameAction);
    }

    #endregion

    #region Protected Methods

    protected override void ChangeState(SeparatorState newState)
    {
        base.ChangeState(newState);
        if (newState == Generating)
            TimedAction.RestartAction(winGameAction);
    }

    #endregion

}


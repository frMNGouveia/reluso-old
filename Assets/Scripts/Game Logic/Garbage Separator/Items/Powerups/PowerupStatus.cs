public class PowerupStatus : IPowerupStatus
{

    public PowerupItem powerup{ get; set; }
    public bool active { get; set; }
    public TimedAction deactivateAction { get; set; }

    public PowerupStatus()
    {
    }

    public bool IsActive()
    {
        return active;
    }

    public float GetTimeLeft()
    {
        if (deactivateAction != null)
            return deactivateAction.GetTimeLeft();
        else
            return -1f;
    }
}

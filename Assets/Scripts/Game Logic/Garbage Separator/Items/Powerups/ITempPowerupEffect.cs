using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITempPowerupEffect : IPowerupEffect
{

    void DeactivatePowerup(IGarbageSeparator separator);

}

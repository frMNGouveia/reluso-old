
public interface IPowerupStatus
{

    bool IsActive();

    float GetTimeLeft();

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPowerupEffect
{

    void ActivatePowerup(IGarbageSeparator separator);

}

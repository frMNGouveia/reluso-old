﻿/**
 * Created By: Francisco Miguel Nunes Gouveia
 * Created On: 
*/
using UnityEngine;

public class SeparatedTrashItem
{

    #region Variable Fields

    public TrashItem TrashItem { get; }
    public Container Container { get;  }
    public bool CorrectlySeparated { get; }

    #endregion

    #region Constructor Methods

    public SeparatedTrashItem(TrashItem trashItem, Container container)
    {
        this.TrashItem = trashItem;
        this.Container = container;
        this.CorrectlySeparated = WasCorrectlySeparated();
    }

    #endregion

    #region Constructor Methods

    public bool WasCorrectlySeparated()
    {
        if (Container == null)
            return false;
        else
            return Container.ContainsType(TrashItem.trashType);
    }

    #endregion

}

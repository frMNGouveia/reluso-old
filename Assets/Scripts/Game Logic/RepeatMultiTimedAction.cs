using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatMultiTimedAction : MultiTimedAction
{

    #region Constructor Methods

    public RepeatMultiTimedAction(List<TimedAction> actions) : base(actions)
    {
        if (base.actions.Count > 0)
        {
            int index = base.actions.Count - 1;
            TimedAction timedAction = base.actions[index];
            base.actions[index] =
                TimedAction.CreateAction(() => DoAndRepeat(timedAction, this), timedAction.GetDelay());
        }
    }

    #endregion

    #region Static Methods

    private static void DoAndRepeat(TimedAction action, RepeatMultiTimedAction repeat)
    {
        action.GetAction()();
        repeat.StartAll();
    }

    #endregion

}

using System;
using System.Collections.Generic;

public class MultiTimedAction
{

    #region Variable Fields

    protected List<TimedAction> actions;
    protected int currentIndex;

    #endregion

    #region Constructor Methods

    public MultiTimedAction(List<TimedAction> actions)
    {
        this.actions = new List<TimedAction>();
        if(this.actions != null)
        {
            for (int i = 0; i < actions.Count - 1; i++)
                this.actions.Add(TimedAction.CreateAction(() => DoThenNext(actions[i], this),
                    actions[i].GetDelay()));
            if (actions.Count > 0)
                this.actions.Add(actions[actions.Count - 1]);
        }
    }

    #endregion

    #region Public Methods

    public void StartAll()
    {
        currentIndex = 0;
        if (HasActions())
            TimedAction.RestartAction(CurrentAction());
    }

    public void StopAll()
    {
        foreach (TimedAction timedAction in actions)
            TimedAction.StopAction(timedAction);
    }

    public void PauseAll()
    {
        if (HasActions())
            TimedAction.PauseAction(CurrentAction());
    }

    public void UnpauseAll()
    {
        if (HasActions())
            TimedAction.UnpauseAction(CurrentAction());
    }

    #endregion

    #region Public Methods

    private bool HasActions()
    {
        return actions != null && actions.Count > 0;
    }

    private TimedAction CurrentAction()
    {
        return actions[currentIndex];
    }


    private static void DoThenNext(TimedAction action, MultiTimedAction multi)
    {
        action.GetAction()();
        TimedAction.RestartAction(multi.actions[++multi.currentIndex]);
    }

    #endregion

}

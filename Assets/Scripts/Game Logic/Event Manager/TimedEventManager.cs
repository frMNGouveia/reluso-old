using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedEventManager : ITimedEventManager
{

    #region Variable Fields

    private float currentTime;
    private long currentIndex;
    private IDictionary<long, IList<ITimedEvent>> events;

    #endregion

    #region Constructor Methods

    public TimedEventManager()
    {
        currentTime = 0f;
        currentIndex = 0;
        events = new Dictionary<long, IList<ITimedEvent>>();
    }

    #endregion

    #region Public Methods

    public void AddTimedEvent(ITimedEvent timedEvent)
    {
        AddEventToDictionary(timedEvent);
    }

    public ICollection<ITimedEvent> UpdateTime(float deltaTime)
    {
        currentTime += deltaTime;
        long oldTime = currentIndex;
        currentIndex = Convert.ToInt64(deltaTime + currentIndex);

        List<ITimedEvent> triggeredEvents = new List<ITimedEvent>();

        for (long i = oldTime; i <= currentIndex; i++)
            if(events.ContainsKey(i))
                triggeredEvents.AddRange(events[i]);

        return triggeredEvents;
    }
    
    public ICollection<ITimedEvent> ScheduledEvents()
    {


        //TODO: THIS
        return null;
    }

    public float GetTimeLeftUntilEvent(ITimedEvent e)
    {
        //TODO: THIS

        return 0f;
    }

    public float GetCurrentTimeInSeconds()
    {
        return currentIndex;
    }

    #endregion

    #region Private Methods

    private void AddEventToDictionary(ITimedEvent timedEvent)
    {
        long timestamp = Convert.ToInt64(timedEvent.GetTriggerTimestamp()) + currentIndex;
        IList<ITimedEvent> eventsAtStamp = events[timestamp];
        if (eventsAtStamp == null)
        {
            eventsAtStamp = new List<ITimedEvent>();
            events.Add(timestamp, eventsAtStamp);
        }
        eventsAtStamp.Add(timedEvent);
    }

    #endregion

}

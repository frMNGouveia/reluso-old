using static GarbageSeparator;

public class EndGameEvent : TimedEvent
{

    #region Variable Fields

    private IGarbageSeparator separator;
    private EndState endState;

    #endregion


    #region Constructor Methods

    public EndGameEvent(float timestamp, IGarbageSeparator separator, EndState endState) : base(timestamp)
    {
        this.separator = separator;
        this.endState = endState;
    }

    #endregion

    #region Public Methods


    public override void OnEventTriggered()
    {
        separator.EndGame(endState);
    }

    #endregion
}

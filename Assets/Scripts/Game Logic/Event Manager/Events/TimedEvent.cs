using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TimedEvent : ITimedEvent
{

    #region Variable Fields

    private float timestamp;

    #endregion

    #region Constructor Methods

    public TimedEvent(float timestamp)
    {
        this.timestamp = timestamp;
    }

    #endregion

    #region Public Methods

    public float GetTriggerTimestamp()
    {
        return timestamp;
    }

    public abstract void OnEventTriggered();

    #endregion

}

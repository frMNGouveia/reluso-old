using static GarbageSeparator;

public class EndPowerupEvent : TimedEvent
{

    #region Constructor Methods

    private IGarbageSeparator separator;
    private PowerupItem item;

    #endregion

    #region Constructor Methods

    public EndPowerupEvent(float timestamp, IGarbageSeparator separator, PowerupItem item) : base(timestamp)
    {
        this.separator = separator;
        this.item = item;
    }

    #endregion

    #region Public Methods

    public override void OnEventTriggered()
    {
        separator.DeactivatePowerup(item);
    }

    #endregion

}

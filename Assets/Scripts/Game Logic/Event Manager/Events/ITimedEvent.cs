using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITimedEvent
{

    void OnEventTriggered();

    float GetTriggerTimestamp();

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITimedEventManager
{

    void AddTimedEvent(ITimedEvent timedEvent);

    ICollection<ITimedEvent> UpdateTime(float deltaTime);

    ICollection<ITimedEvent> ScheduledEvents();

    float GetTimeLeftUntilEvent(ITimedEvent e);

    float GetCurrentTimeInSeconds();

}
